<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Mode\User;
use App\Http\Controllers\Controller;
use App\Model\Logs;
use DB;

class HospitalChargeController extends Controller
{
    
    public function hospitalcharge()
    {

        $data["hospital_charge"] = DB::table('hospital_charge')
        ->orderBy('id','desc')
        ->get();

        // echo "<pre>";
        // print_r($data);
        // exit();

        return view('admin.hospitalcharge.hospitalcharge', $data);
    }

    
    public function hospitalchargeadd()
    {


        // echo "<pre>";
        // print_r($data);
        // exit();

        return view('admin.hospitalcharge.hospitalchargeadd');
    }

    public function hospitalchargeac(Request $request)
    {

        $inputData = [
            'charge_name' => $request->charge_name,
            'amount' => $request->amount
            
        ];


        DB::table('hospital_charge')
        ->insert($inputData);

        // echo "<pre>";
        // print_r($data);
        // exit();

        $flashdata = ['class'=>'success', 'message'=>"Add Successfull "];
        return redirect()->back()->with($flashdata);
    }

    
    public function hospitalchargedelete($id)
    {

        DB::table('hospital_charge')->where('id', $id)->delete();

        // echo "<pre>";
        // print_r($data);
        // exit();

        $flashdata = ['class'=>'danger', 'message'=>"Delete Successfull "];
        return redirect()->back()->with($flashdata);
    }

}
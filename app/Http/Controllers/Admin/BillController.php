<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use App\Mode\User;
use App\Http\Controllers\Controller;
use App\Model\Logs;
use DB;
use Auth;



class BillController extends Controller
{
    
    public function bill()
    {

        $data["bill_manage"] = DB::table('bill_manage')
        ->orderBy('id','desc')
        ->get();

        // echo "<pre>";
        // print_r($data);
        // exit();

        return view('admin.bill.bill', $data);
    }

    
    public function billadd()
    {

        $data["bed_manage"] = DB::table('bed_manage')
        ->orderBy('id','desc')
        ->get();

        $data["patient_manage"] = DB::table('patient_manage')
        ->orderBy('id','desc')
        ->get();

        $data["doctor_manage"] = DB::table('doctor_manage')
        ->orderBy('id','desc')
        ->get();

        $data["hospital_charge"] = DB::table('hospital_charge')
        ->orderBy('id','desc')
        ->get();

        $data["investigation_manage"] = DB::table('investigation_manage')
        ->orderBy('id','asc')
        ->get();

        $bill_manage = $data["bill_manage"] = DB::table('bill_manage')
        ->orderBy('id','desc')
        ->first();

            if(empty($bill_manage)) {
            $billno = 0;
            $data['billid'] = Auth::guard('user')->user()->id.$billno+1;
            $nextno = Auth::guard('user')->user()->id.$billno+1;
            }else{
            $data['billid'] = Auth::guard('user')->user()->id.$bill_manage->id+1;    
            $nextno = Auth::guard('user')->user()->id.$bill_manage->id+1;    
            }

        $salesItem = $data['salesItem'] = DB::table('bill_add_to_cart')
        ->where('bill_id', $nextno)
        ->orderBy('id', 'asc')
        ->get();

        //$data['total'] = $total;
        // echo "<pre>";
        // print_r($data);
        // exit();

        return view('admin.bill.billadd', $data);
    }

    
    public function charge_show(Request $request)
    {
        $charge_id = $request->charge_id;

        $hospital_charge = DB::table('hospital_charge')
        ->where('id',$charge_id)
        ->orderBy('id','asc')
        ->first();

        // echo "<pre>";
        // print_r($cou_package_manage);
        // exit();

        $html = '<div class="row">

<div class="col-md-5">
<div class="form-group">
  <input class="form-control" type="text" id="charge_name" name="charge_name" readonly="" value="'.$hospital_charge->charge_name.'">
</div>
</div>

<div class="col-md-2">
<div class="form-group">
  <input class="form-control" type="Number" id="qty" name="qty" placeholder="Qty" value="1">
</div>
</div>

<div class="col-md-3">
<div class="form-group">
  <input class="form-control" type="Number" id="amount" name="amount" placeholder="Type Price" value="'.$hospital_charge->amount.'">
  <input class="form-control" type="hidden" id="c_type" name="c_type" placeholder="Type Price" value="0">
  <input class="form-control" type="hidden" id="in_id" name="in_id" placeholder="Type Price" value="0">
</div>
</div>

<div class="col-md-2">
<div class="form-group">
<button type="button" class="btn btn-block btn-primary" onclick="add_item_to_cart();">
<i class="fa fa-plus"> Add </i>
</button>
</div>
</div>

</div>';

        return $html;

    }



public function investigation_show(Request $request)
    {
        $investigation_id = $request->investigation_id;

        $investigation_manage = DB::table('investigation_manage')
        ->where('id',$investigation_id)
        ->orderBy('id','asc')
        ->first();

        
        $charge = $investigation_manage->charge;
        $discount = $investigation_manage->discount;
        $total_chr = $investigation_manage->charge;
        // echo "<pre>";
        // print_r($cou_package_manage);
        // exit();

        $html = '<div class="row">

<div class="col-md-5">
<div class="form-group">
  <input class="form-control" type="text" id="charge_name" name="charge_name" readonly="" value="'.$investigation_manage->code_test.' - '.$investigation_manage->inv_name.'">
</div>
</div>

<div class="col-md-2">
<div class="form-group">
  <input class="form-control" type="Number" id="qty" name="qty" placeholder="Qty" value="1">
</div>
</div>

<div class="col-md-3">
<div class="form-group">
  <input class="form-control" type="Number" id="amount" name="amount" placeholder="Type Price" value="'.$total_chr.'">
  <input class="form-control" type="hidden" id="c_type" name="c_type" placeholder="Type Price" value="1">
  <input class="form-control" type="hidden" id="in_id" name="in_id" placeholder="Type Price" value="'.$investigation_manage->discount.'">
</div>
</div>

<div class="col-md-2">
<div class="form-group">
<button type="button" class="btn btn-block btn-primary" onclick="add_item_to_cart();">
<i class="fa fa-plus"> Add </i>
</button>
</div>
</div>

</div>';

        return $html;

    }


public function add_item_to_cart(Request $request)
    {

        $inputData = [
            'bill_id' => $request->bill_id,
            'charge_name' => $request->charge_name,
            'qty' => $request->qty,
            'amount' => $request->amount,
            'total' => $request->qty * $request->amount,
            'c_type' => $request->c_type,
            'in_id' => $request->in_id
            
        ];


        DB::table('bill_add_to_cart')
        ->insert($inputData);

        // echo "<pre>";
        // print_r($data);
        // exit();

$salesItem = DB::table('bill_add_to_cart')
->where('bill_id', $request->bill_id)
->orderBy('id', 'asc')
->get();

        $html = '<table class="table table-bordered scrollContent">
          <thead>
            <tr>
              <th width="55%">Details</th>
              <th width="15%">Qty</th>
              <th width="15%">Rate</th>
              <th width="15%">Total </th>
              <th width="10%">Action</th>
            </tr>
          </thead> <tbody>';
          foreach ($salesItem as $key => $data) {
           $html.= '<tr>
                  <td>'.$data->charge_name.'</td>
                  <td>'.$data->qty.'</td>
                  <td>'.$data->amount.'</td>
                  <td>'.$data->total.'</td>
                  <td><button type="button" class="btn btn-danger btn-sm waves-effect waves-light" onclick="deletefn(this)" data-id="'.$data->id.'"><i class="fa fa-times"></i>
          </button></td>
                </tr>';
            }


           $html.= '</tbody>
</table>';


return $html;  


}


public function delete_from_cart(Request $request)
    {
        $id = $request->data;
        $bill_id = $request->bill_id;

        DB::table('bill_add_to_cart')
        ->where('id', $id)
        ->where('bill_id', $bill_id)
        ->delete();

$salesItem = DB::table('bill_add_to_cart')
->where('bill_id', $request->bill_id)
->orderBy('id', 'asc')
->get();

        $html = '<table class="table table-bordered scrollContent">
          <thead>
            <tr>
              <th width="55%">Details</th>
              <th width="15%">Qty</th>
              <th width="15%">Rate</th>
              <th width="15%">Total </th>
              <th width="10%">Action</th>
            </tr>
          </thead> <tbody>';
          foreach ($salesItem as $key => $data) {
           $html.= '<tr>
                  <td>'.$data->charge_name.'</td>
                  <td>'.$data->qty.'</td>
                  <td>'.$data->amount.'</td>
                  <td>'.$data->total.'</td>
                  <td><button type="button" class="btn btn-danger btn-sm waves-effect waves-light" onclick="deletefn(this)" data-id="'.$data->id.'"><i class="fa fa-times"></i>
          </button></td>
                </tr>';
            }


           $html.= '</tbody>
</table>';


return $html;  


}


public function calculate_total(Request $request)
{

$bill_id = $request->bill_id;

$total = DB::table('bill_add_to_cart')
->where('bill_id', $request->bill_id)
->orderBy('id', 'asc')
->sum('total');

$html = $total;
  
return $html;  


}


public function hc_rf_calculate_total(Request $request)
{

$bill_id = $request->bill_id;

$total = DB::table('bill_add_to_cart')
->where('bill_id', $request->bill_id)
->where('c_type',0)
->orderBy('id', 'asc')
->sum('total');

$html = $total;
  
return $html;

}


public function billaction(Request $request)
{

date_default_timezone_set('Asia/Dhaka');
$date = date("d/m/Y");
$date_time = date("d/m/Y h:i:s a");

$bill_manage = $data["bill_manage"] = DB::table('bill_manage')
->orderBy('id','desc')
->first();

    if(empty($bill_manage)) {
    $billno = 0;
    $data['billid'] = Auth::guard('user')->user()->id.$billno+1;
    $nextno = Auth::guard('user')->user()->id.$billno+1;
    }else{
    $data['billid'] = Auth::guard('user')->user()->id.$bill_manage->id+1;    
    $nextno = Auth::guard('user')->user()->id.$bill_manage->id+1;    
    }

    $grand_total  = $request->totalval - $request->inDiscount;

    $due = $grand_total - $request->inPayment;

    $hc_rf = DB::table('bill_add_to_cart')
    ->where('c_type',0)
    ->where('bill_id',$nextno)
    ->sum('total');

    $inv_ch_rf = DB::table('bill_add_to_cart')
    ->where('c_type',1)
    ->where('bill_id',$nextno)
    ->get();

    $inv_ch_per = 0;

    foreach($inv_ch_rf as $data){
      $inv_ch_per += ($data->total * $data->in_id) / 100;
    }

    $inv_ch_per_tot = $inv_ch_per - $request->inDiscount;

    $inputData = [
            'bill_id' => $nextno,
            'date_time' => $date_time,
            'date' => $date,
            'patient_id' => $request->patient_id,
            'visit_dr'  => $request->visit_dr,
            'dr_id' => $request->dr_id,
            'bed_id' => $request->bed_id,
            'total' => $request->totalval,
            'discount' => $request->inDiscount,
            'grand_total' => $grand_total,
            'payment' => $request->inPayment,
            'due' => $due,
            'status' => $request->pay_status,
            'hc_rf_tot' => $hc_rf,
            'hc_rf_fee' => $request->reffee,
            'inv_rf_fee' => $inv_ch_per_tot
        ];

    DB::table('bill_manage')
    ->insert($inputData);

$patient_id = DB::table('patient_manage')
->where('id',$request->patient_id)
->first();

$pat_id = $patient_id->pat_id;
$pat_name = $patient_id->name;
$pat_mobile = $patient_id->phone;

$doctor_manage_count = DB::table('doctor_manage')
->where('id',$request->dr_id)
->count();

if($doctor_manage_count == 1){

$doctor_manage = DB::table('doctor_manage')
->where('id',$request->dr_id)
->first();

$dr_phone = $doctor_manage->dr_phone;
$mr_id = $doctor_manage->mr_id;
$mr_name = $doctor_manage->mr_name;
$mr_com = $doctor_manage->mr_com;

$ref_fee = $request->reffee + $inv_ch_per;

} else {

  $ref_fee = 0;
  $mr_id = "N/A";
  $mr_name = "N/A";

}


$settings = DB::table('settings')
->where('id',1)
->first();

$admin_mobile = $settings->phone;

//SMS 1 = PATIENT

$smstext = urlencode("Dear ".$pat_name. " (".$pat_id. "). Your Total Bill : ".$grand_total. "TK. Thanks M.H.P. ");

file_get_contents("http://sms.tense.com.bd/api-sendsms?user=mrcourier&password=01716110032&campaign=MRCOURIER&number=".$pat_mobile."&text=".$smstext);

if($doctor_manage_count == 1){

//SMS 2 = REF MR

$smstext1 = urlencode("Your Patient ".$pat_name. " (".$pat_id. "). Total Bill : ".$grand_total. "TK Ref Fee : ".$ref_fee. "TK MR ".$mr_name. " (".$mr_id. "). Thanks M.H.P. ");

file_get_contents("http://sms.tense.com.bd/api-sendsms?user=mrcourier&password=01716110032&campaign=MRCOURIER&number=".$dr_phone."&text=".$smstext1);

}

//SMS 3 = ADMIN 

$smstext1 = urlencode("Dear Admin, Bill No : ".$nextno. " Date Time : ".$date. " Patient ".$pat_name. " (".$pat_id. "). Total Bill : ".$grand_total. "TK Ref Fee : ".$ref_fee. "TK MR ".$mr_name. " (".$mr_id. "). Thanks M.H.P. ");

file_get_contents("http://sms.tense.com.bd/api-sendsms?user=mrcourier&password=01716110032&campaign=MRCOURIER&number=".$admin_mobile."&text=".$smstext1);


// $flashdata = ['class'=>'success', 'message'=>"Bill Generate Successfull "];
// return redirect()->back()->with($flashdata);

return \Redirect::route('Admin.billprint', $nextno);

}

public function billprint($id)
{

$data["bill_manage"] = DB::table('bill_manage')
->join('patient_manage', 'patient_manage.id', '=', 'bill_manage.patient_id')
->leftjoin('doctor_manage', 'doctor_manage.id', '=', 'bill_manage.dr_id')
->leftjoin('bed_manage', 'bed_manage.id', '=', 'bill_manage.bed_id')
->select('bill_manage.*', 
  'patient_manage.pat_type',
  'patient_manage.pat_id',
  'patient_manage.name',
  'patient_manage.age',
  'patient_manage.gender',
  'patient_manage.phone',
  'patient_manage.address',
  'patient_manage.gur_name',
  'patient_manage.remarks',
  'patient_manage.address',
  'doctor_manage.dr_code',
  'doctor_manage.dr_name',
  'doctor_manage.dr_phone',
  'bed_manage.bed_name',
  'bed_manage.bed_type',
  'bed_manage.floor'
)
->where('bill_manage.bill_id',$id)
->first();

$data["bill_add_to_cart"] = DB::table('bill_add_to_cart')
->where('bill_add_to_cart.bill_id',$id)
->get();

return view('admin.bill.billprint', $data);
}



    
    public function billpayment($id)
    {

        $data["bill_manage"] = $bill_manage = DB::table('bill_manage')
        ->leftjoin('patient_manage', 'patient_manage.id', '=', 'bill_manage.patient_id')
        ->leftjoin('doctor_manage', 'doctor_manage.id', '=', 'bill_manage.dr_id')
        ->leftjoin('bed_manage', 'bed_manage.id', '=', 'bill_manage.bed_id')
        ->select('bill_manage.*', 
            'patient_manage.pat_type',
            'patient_manage.pat_id',
            'patient_manage.name',
            'patient_manage.age',
            'patient_manage.gender',
            'patient_manage.phone',
            'patient_manage.address',
            'patient_manage.gur_name',
            'patient_manage.remarks',
            'patient_manage.address',
            'doctor_manage.dr_code',
            'doctor_manage.dr_name',
            'doctor_manage.dr_phone',
            'doctor_manage.mr_id',
            'doctor_manage.mr_name',
            'bed_manage.bed_name',
            'bed_manage.bed_type',
            'bed_manage.floor'
          )
        ->where('bill_manage.bill_id',$id)
        ->first();

        // echo "<pre>";
        // print_r($bill_manage);
        // exit();


        $salesItem = $data['salesItem'] = DB::table('bill_add_to_cart')
        ->where('bill_id', $id)
        ->orderBy('id', 'asc')
        ->get();





        return view('admin.bill.billpayment', $data);
    }

    public function billpaymentaction(Request $request)
    {
          date_default_timezone_set('Asia/Dhaka');
          $date = date("d/m/Y h:i:s a");

          $id = $request->id;
          $pay_amount = $request->pay_amount;
          $pay_status = $request->pay_status;

          $data["bill_manage"] = $bill_manage = DB::table('bill_manage')
          ->where('bill_manage.bill_id',$id)
          ->first();

          $due = $bill_manage->due;

          $due_total = $due - $pay_amount;

          $inputData = [
                'payment' => $pay_amount,
                'due' => $due_total,
                'status' => $pay_status,
                'due_bill_date' => $date
            ];

        DB::table('bill_manage')
        ->where("bill_id",$id)
        ->update($inputData);

        return \Redirect::route('Admin.billprint', $id);
    }

}
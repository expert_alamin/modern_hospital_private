<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use App\Mode\User;
use App\Http\Controllers\Controller;
use App\Model\Logs;
use DB;
use Auth;


class PharmacyConroller extends Controller
{
    
    public function phaproductcategory()
    {

        $data["pha_category"] = DB::table('pha_category')
        ->orderBy('id','desc')
        ->get();

        // echo "<pre>";
        // print_r($data);
        // exit();

        return view('admin.pharmacy.phaproductcategory', $data);
    }


public function phaproductcategoryadd(Request $request)
{

date_default_timezone_set('Asia/Dhaka');
$date = date("d/m/Y h:i:s a");


    $inputData = [
            'category_name' => $request->category_name
        ];

    DB::table('pha_category')
    ->insert($inputData);

$flashdata = ['class'=>'success', 'message'=>"Category Successfull "];
return redirect()->back()->with($flashdata);

}


public function pharcatedit($id)
{
  $data["pha_category"] = DB::table('pha_category')
        ->where('id',$id)
        ->first();

        // echo "<pre>";
        // print_r($data);
        // exit();

        return view('admin.pharmacy.pharcatedit', $data);
}


public function phaproductcategoryeditac(Request $request)
{

date_default_timezone_set('Asia/Dhaka');
$date = date("d/m/Y h:i:s a");


    $inputData = [
            'category_name' => $request->category_name
        ];

    DB::table('pha_category')
    ->where('id',$request->id)
    ->update($inputData);

$flashdata = ['class'=>'success', 'message'=>"Category Update Successfull "];
return redirect()->back()->with($flashdata);

}

public function pharcatdel($id)
    {

        DB::table('pha_category')->where('id', $id)->delete();

        // echo "<pre>";
        // print_r($data);
        // exit();

        $flashdata = ['class'=>'danger', 'message'=>"Category Delete Successfull "];
        return redirect()->back()->with($flashdata);
    }


public function phamedicinecompany()
{

    $data["pha_medicine_comapany"] = DB::table('pha_medicine_comapany')
    ->orderBy('id','desc')
    ->get();

    // echo "<pre>";
    // print_r($data);
    // exit();

    return view('admin.pharmacy.phamedicinecompany', $data);
}




public function phamedicinecompanyadd(Request $request)
{

date_default_timezone_set('Asia/Dhaka');
$date = date("d/m/Y h:i:s a");


    $inputData = [
            'company_name' => $request->company_name
        ];

    DB::table('pha_medicine_comapany')
    ->insert($inputData);

$flashdata = ['class'=>'success', 'message'=>"Medicine Company Successfull "];
return redirect()->back()->with($flashdata);

}

public function phamedicinecompanydelete($id)
    {

        DB::table('pha_medicine_comapany')->where('id', $id)->delete();

        // echo "<pre>";
        // print_r($data);
        // exit();

        $flashdata = ['class'=>'danger', 'message'=>"Medicine Company Delete Successfull "];
        return redirect()->back()->with($flashdata);
    }


public function phamedpurlist()
{


    $pha_medicine_purchase = $data["pha_medicine_purchase"] = DB::table('pha_medicine_purchase')
    ->orderBy('id','desc')
    ->get();

    return view('admin.pharmacy.phamedpurlist', $data);
}

public function phamedpur()
{

    $data["pha_category"] = DB::table('pha_category')
    ->orderBy('id','desc')
    ->get();

    $data["pha_medicine_comapany"] = DB::table('pha_medicine_comapany')
    ->orderBy('id','desc')
    ->get();

    $pha_medicine_purchase = $data["pha_medicine_purchase"] = DB::table('pha_medicine_purchase')
        ->orderBy('id','desc')
        ->first();

            if(empty($pha_medicine_purchase)) {
            $billno = 0;
            $data['billid'] = Auth::guard('user')->user()->id.$pha_medicine_purchase+1;
            $nextno = Auth::guard('user')->user()->id.$pha_medicine_purchase+1;
            }else{
            $data['billid'] = Auth::guard('user')->user()->id.$pha_medicine_purchase->id+1;    
            $nextno = Auth::guard('user')->user()->id.$pha_medicine_purchase->id+1;    
            }

$data['salesItem'] =$salesItem = DB::table('pha_pur_add_to_cart')
->where('bill_id', $nextno)
->orderBy('id', 'asc')
->get();

    // echo "<pre>";
    // print_r($data);
    // exit();

    return view('admin.pharmacy.phamedpur', $data);
}


public function pha_pur_med_add_item_to_cart(Request $request)
    {

      $pha_medicine_purchase = $data["pha_medicine_purchase"] = DB::table('pha_medicine_purchase')
        ->orderBy('id','desc')
        ->first();

            if(empty($pha_medicine_purchase)) {
            $billno = 0;
            $data['billid'] = Auth::guard('user')->user()->id.$pha_medicine_purchase+1;
            $nextno = Auth::guard('user')->user()->id.$pha_medicine_purchase+1;
            }else{
            $data['billid'] = Auth::guard('user')->user()->id.$pha_medicine_purchase->id+1;    
            $nextno = Auth::guard('user')->user()->id.$pha_medicine_purchase->id+1;    
            }

        $inputData = [
            'bill_id' => $nextno,
            'serial_number' => $request->serial_number,
            'medicine_name' => $request->medicine_name,
            'qty' => $request->qty,
            'buy_price' => $request->buy_price,
            'totay_buy_price' => $request->qty * $request->buy_price,
            'sell_price' => $request->sell_price
        ];


        DB::table('pha_pur_add_to_cart')
        ->insert($inputData);

        // echo "<pre>";
        // print_r($data);
        // exit();

$salesItem = DB::table('pha_pur_add_to_cart')
->where('bill_id', $nextno)
->orderBy('id', 'asc')
->get();

        $html = '<table class="table table-bordered scrollContent">
          <thead>
            <tr>
              <th style="width: 25%">Serial</th>
              <th style="width: 40%">Medicine</th>
              <th style="width: 10%">Qty</th>
              <th style="width: 15%">Buy Price</th>
              <th style="width: 15%">Total Buy </th>
              <th style="width: 15%">Sell Price </th>
              <th style="width: 10%">Action</th>
            </tr>
          </thead> <tbody>';
          foreach ($salesItem as $key => $data) {
           $html.= '<tr>
                  <td>'.$data->serial_number.'</td>
                  <td>'.$data->medicine_name.'</td>
                  <td>'.$data->qty.'</td>
                  <td>'.$data->buy_price.'</td>
                  <td>'.$data->totay_buy_price.'</td>
                  <td>'.$data->sell_price.'</td>
                  <td><button type="button" class="btn btn-danger btn-sm waves-effect waves-light" onclick="deletefn(this)" data-id="'.$data->id.'"><i class="fa fa-times"></i>
          </button></td>
                </tr>';
            }


           $html.= '</tbody>
</table>';


return $html;  


}


public function pha_pur_med_delete_from_cart(Request $request)
    {
        $id = $request->data;

        $pha_medicine_purchase = $data["pha_medicine_purchase"] = DB::table('pha_medicine_purchase')
        ->orderBy('id','desc')
        ->first();

            if(empty($pha_medicine_purchase)) {
            $billno = 0;
            $data['billid'] = Auth::guard('user')->user()->id.$pha_medicine_purchase+1;
            $nextno = Auth::guard('user')->user()->id.$pha_medicine_purchase+1;
            }else{
            $data['billid'] = Auth::guard('user')->user()->id.$pha_medicine_purchase->id+1;    
            $nextno = Auth::guard('user')->user()->id.$pha_medicine_purchase->id+1;    
            }

        DB::table('pha_pur_add_to_cart')
        ->where('id', $id)
        ->where('bill_id', $nextno)
        ->delete();

$salesItem = DB::table('bill_add_to_cart')
->where('bill_id', $nextno)
->orderBy('id', 'asc')
->get();

         $html = '<table class="table table-bordered scrollContent">
          <thead>
            <tr>
              <th style="width: 25%">Serial</th>
              <th style="width: 40%">Medicine</th>
              <th style="width: 10%">Qty</th>
              <th style="width: 15%">Buy Price</th>
              <th style="width: 15%">Total Buy </th>
              <th style="width: 15%">Sell Price </th>
              <th style="width: 10%">Action</th>
            </tr>
          </thead> <tbody>';
          foreach ($salesItem as $key => $data) {
           $html.= '<tr>
                  <td>'.$data->serial_number.'</td>
                  <td>'.$data->medicine_name.'</td>
                  <td>'.$data->qty.'</td>
                  <td>'.$data->buy_price.'</td>
                  <td>'.$data->totay_buy_price.'</td>
                  <td>'.$data->sell_price.'</td>
                  <td><button type="button" class="btn btn-danger btn-sm waves-effect waves-light" onclick="deletefn(this)" data-id="'.$data->id.'"><i class="fa fa-times"></i>
          </button></td>
                </tr>';
            }


           $html.= '</tbody>
</table>';

return $html;  

}


public function phamedpuraction(Request $request)
{

date_default_timezone_set('Asia/Dhaka');
$date = date("d/m/Y h:i:s a");

$pha_medicine_purchase = $data["pha_medicine_purchase"] = DB::table('pha_medicine_purchase')
->orderBy('id','desc')
->first();

            if(empty($pha_medicine_purchase)) {
            $billno = 0;
            $data['billid'] = Auth::guard('user')->user()->id.$pha_medicine_purchase+1;
            $nextno = Auth::guard('user')->user()->id.$pha_medicine_purchase+1;
            }else{
            $data['billid'] = Auth::guard('user')->user()->id.$pha_medicine_purchase->id+1;    
            $nextno = Auth::guard('user')->user()->id.$pha_medicine_purchase->id+1;    
            }

$pha_pur_add_to_cart = $data["pha_pur_add_to_cart"] = DB::table('pha_pur_add_to_cart')
->where('bill_id',$nextno)
->get();

$total = 0;
foreach($pha_pur_add_to_cart as $data){
  $total += $data->totay_buy_price;
}

    $inputData = [
            'bill_id' => $nextno,
            'inv_num' => $request->inv_num,
            'date' => $request->date,
            'supplier_name' => $request->supplier_name,
            'supplier_phone' => $request->supplier_phone,
            'product_category' => $request->category_name,
            'company_name' => $request->company_name,
            'total' => $total
        ];

    DB::table('pha_medicine_purchase')
    ->insert($inputData);

return \Redirect::route('Admin.phamedpurbill', $nextno);

}



public function phamedpurbill($id)
{

$data["bill_manage"] = DB::table('pha_medicine_purchase')
->where('bill_id',$id)
->first();

$data["pha_pur_add_to_cart"] = DB::table('pha_pur_add_to_cart')
->where('bill_id',$id)
->get();

return view('admin.pharmacy.phamedpurbill', $data);
}


public function phamedpurbilledit($id)
{

$pha_sales_manage = $data["pha_sales_manage"] = DB::table('pha_medicine_purchase')
->where('bill_id',$id)
->first();

// echo "<pre>";
// print_r($pha_sales_manage);
// exit();
    return view('admin.pharmacy.phamedpurbilledit', $data);
}


        
public function phamedpureditac(Request $request){

date_default_timezone_set('Asia/Dhaka');
$date = date("d/m/Y h:i:s a");

    $inputData = [
      'product_category' => $request->product_category
    ];

    DB::table('pha_medicine_purchase')
    ->where('id',$request->id)
    ->update($inputData);

$flashdata = ['class'=>'success', 'message'=>"Medicine Category Update "];
return redirect()->back()->with($flashdata);


}

public function phamedselllist()
{


    $pha_sales_manage = $data["pha_sales_manage"] = DB::table('pha_sales_manage')
    ->orderBy('id','desc')
    ->get();

    return view('admin.pharmacysell.phamedselllist', $data);
}


public function phamedsell(){

  $data["pha_pur_add_to_cart"] = DB::table('pha_pur_add_to_cart')
  ->join('pha_medicine_purchase', 'pha_medicine_purchase.bill_id', '=', 'pha_pur_add_to_cart.bill_id')
  ->select('pha_pur_add_to_cart.*', 'pha_medicine_purchase.product_category')
  ->orderBy('id','desc')
  ->get();

    $data["patient_manage"] = DB::table('patient_manage')
        ->orderBy('id','desc')
        ->get();

    $data["bed_manage"] = DB::table('bed_manage')
        ->orderBy('id','desc')
        ->get();

  $pha_sales_manage = $data["pha_sales_manage"] = DB::table('pha_sales_manage')
  ->orderBy('id','desc')
  ->first();

            if(empty($pha_sales_manage)) {
            $billno = 0;
            $data['billid'] = Auth::guard('user')->user()->id.$pha_sales_manage+1;
            $nextno = Auth::guard('user')->user()->id.$pha_sales_manage+1;
            }else{
            $data['billid'] = Auth::guard('user')->user()->id.$pha_sales_manage->id+1;    
            $nextno = Auth::guard('user')->user()->id.$pha_sales_manage->id+1;    
            }

$data['salesItem'] =$salesItem = DB::table('pha_sales_add_to_cart')
->where('bill_id', $nextno)
->orderBy('id', 'asc')
->get();

return view('admin.pharmacysell.phamedsell', $data);

}



public function medicinesellshow(Request $request)
{
  $medicine_id = $request->medicine_id;

  $MediItem = DB::table('pha_pur_add_to_cart')
  ->join('pha_medicine_purchase', 'pha_medicine_purchase.bill_id', '=', 'pha_pur_add_to_cart.bill_id')
  ->select('pha_pur_add_to_cart.*', 'pha_medicine_purchase.product_category')
  ->where('pha_pur_add_to_cart.id', $medicine_id)
  ->first();

  $instock = $MediItem->qty;

  if($instock >= 1){

    $html = '<div class="row">


<div class="col-md-6">
<div class="form-group">
  <input class="form-control" type="text" id="medicine_name" name="medicine_name" placeholder="Medicine Name" value="'.$MediItem->product_category.'-'.$MediItem->medicine_name.'" readonly>
</div>
</div>

<div class="col-md-2">
<div class="form-group">
  <input class="form-control" type="Number" id="qty" name="qty" placeholder="Qty" value="1">
<br>
<span style="color:#1fa062;font-weight:bold;"> Stock : '.$MediItem->qty.' </span>
</div>
</div>


<div class="col-md-2">
<div class="form-group">
  <input class="form-control" type="Number" id="sell_price" name="sell_price" placeholder="Type Sell Price" value="'.$MediItem->sell_price.'" >
</div>
</div>

<div class="col-md-2">
<div class="form-group">
<button type="button" class="btn btn-block btn-primary" onclick="add_item_to_carts();">
<i class="fa fa-plus"> Add </i>
</button>
</div>
</div>

</div>';


return $html; 

  } else {

     $html = '<div class="row">


<div class="col-md-6">
<div class="form-group">
  <input class="form-control" type="text" id="medicine_name" name="medicine_name" placeholder="Medicine Name" value="'.$MediItem->product_category.'-'.$MediItem->medicine_name.'" readonly>
</div>
</div>

<div class="col-md-2">
<div class="form-group">
<span class="form-control" style="color:#fe3549;font-weight:bold;"> '.$MediItem->qty.' - Out of Stock </span>
</div>
</div>


<div class="col-md-2">
<div class="form-group">
  
</div>
</div>

<div class="col-md-2">
<div class="form-group">
<button type="button" class="btn btn-block btn-danger" >
<i class="fa fa-plus"> Out of Stock </i>
</button>
</div>
</div>

</div>';


return $html; 

  }

        

}



public function med_sales_add_item_to_cart(Request $request)
    {

$pha_sales_manage = $data["pha_sales_manage"] = DB::table('pha_sales_manage')
->orderBy('id','desc')
->first();

            if(empty($pha_sales_manage)) {
            $billno = 0;
            $data['billid'] = Auth::guard('user')->user()->id.$pha_sales_manage+1;
            $nextno = Auth::guard('user')->user()->id.$pha_sales_manage+1;
            }else{
            $data['billid'] = Auth::guard('user')->user()->id.$pha_sales_manage->id+1;    
            $nextno = Auth::guard('user')->user()->id.$pha_sales_manage->id+1;    
            }

        $inputData = [
            'bill_id' => $nextno,
            'med_name' => $request->medicine_name,
            'price' => $request->sell_price,
            'qty' => $request->qty,
            'total' => $request->sell_price * $request->qty
        ];


        DB::table('pha_sales_add_to_cart')
        ->insert($inputData);

        // echo "<pre>";
        // print_r($data);
        // exit();

$salesItem = DB::table('pha_sales_add_to_cart')
->where('bill_id', $nextno)
->orderBy('id', 'asc')
->get();

        $html = '<table class="table table-bordered scrollContent">
          <thead>
            <tr>
              <th style="width: 40%">Medicine</th>
              <th style="width: 10%">Qty</th>
              <th style="width: 15%">Amount </th>
              <th style="width: 15%">Total </th>
              <th style="width: 10%">Action</th>
            </tr>
          </thead> <tbody>';
          $total = 0;
          foreach ($salesItem as $key => $data) {
           $html.= '<tr>
                  <td>'.$data->med_name.'</td>
                  <td>'.$data->qty.'</td>
                  <td>'.$data->price.'</td>
                  <td>'.$data->total.' TK</td>
                  <td><button type="button" class="btn btn-danger btn-sm waves-effect waves-light" onclick="deletefn(this)" data-id="'.$data->id.'"><i class="fa fa-times"></i>
          </button></td>
                </tr>';
                $total += $data->total;
            }
             $html.= '<tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td>'.$total.' TK</td>
                  <td></td>
                </tr>';

           $html.= '</tbody>
</table>';

return $html;  
}



public function med_sales_delete_from_cart(Request $request)
    {
        $id = $request->data;

        $pha_sales_manage = $data["pha_sales_manage"] = DB::table('pha_sales_manage')
  ->orderBy('id','desc')
  ->first();

            if(empty($pha_sales_manage)) {
            $billno = 0;
            $data['billid'] = Auth::guard('user')->user()->id.$pha_sales_manage+1;
            $nextno = Auth::guard('user')->user()->id.$pha_sales_manage+1;
            }else{
            $data['billid'] = Auth::guard('user')->user()->id.$pha_sales_manage->id+1;    
            $nextno = Auth::guard('user')->user()->id.$pha_sales_manage->id+1;    
            }

        DB::table('pha_sales_add_to_cart')
        ->where('id', $id)
        ->where('bill_id', $nextno)
        ->delete();

$salesItem = DB::table('pha_sales_add_to_cart')
->where('bill_id', $nextno)
->orderBy('id', 'asc')
->get();

         $html = '<table class="table table-bordered scrollContent">
          <thead>
            <tr>
              <th style="width: 40%">Medicine</th>
              <th style="width: 10%">Qty</th>
              <th style="width: 15%">Amount </th>
              <th style="width: 15%">Total </th>
              <th style="width: 10%">Action</th>
            </tr>
          </thead> <tbody>';
          $total = 0;
          foreach ($salesItem as $key => $data) {
           $html.= '<tr>
                  <td>'.$data->med_name.'</td>
                  <td>'.$data->qty.'</td>
                  <td>'.$data->price.'</td>
                  <td>'.$data->total.' TK</td>
                  <td><button type="button" class="btn btn-danger btn-sm waves-effect waves-light" onclick="deletefn(this)" data-id="'.$data->id.'"><i class="fa fa-times"></i>
          </button></td>
                </tr>';
                $total += $data->total;
            }
             $html.= '<tr>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td>'.$total.' TK</td>
                  <td></td>
                </tr>';

           $html.= '</tbody>
</table>';

return $html;  

}



public function phamedsalesaction(Request $request)
{

date_default_timezone_set('Asia/Dhaka');
$date = date("d/m/Y");
$date_time = date("d/m/Y h:i:s a");

$pha_sales_manage = $data["pha_sales_manage"] = DB::table('pha_sales_manage')
  ->orderBy('id','desc')
  ->first();

            if(empty($pha_sales_manage)) {
            $billno = 0;
            $data['billid'] = Auth::guard('user')->user()->id.$pha_sales_manage+1;
            $nextno = Auth::guard('user')->user()->id.$pha_sales_manage+1;
            }else{
            $data['billid'] = Auth::guard('user')->user()->id.$pha_sales_manage->id+1;    
            $nextno = Auth::guard('user')->user()->id.$pha_sales_manage->id+1;    
            }

$pha_sales_add_to_cart = $data["pha_sales_add_to_cart"] = DB::table('pha_sales_add_to_cart')
->where('bill_id',$nextno)
->get();

$countpro = $data["pha_sales_add_to_cart"] = DB::table('pha_sales_add_to_cart')
->where('bill_id',$nextno)
->count();

if($countpro != 0){



$total = 0;
foreach($pha_sales_add_to_cart as $data){

  $total += $data->total;
  $med_name = $data->med_name;

  $iparr = preg_split("/[-]+/", $med_name); 
  $pro_med = $iparr[1];

  $product_qty = $data->qty;

$pha_pur_add_to_cart = DB::table('pha_pur_add_to_cart')
->where('medicine_name',$pro_med)
->first();

$product_id = $pha_pur_add_to_cart->id;
$get_cur_stock = $pha_pur_add_to_cart->qty;

$now_qty = $get_cur_stock - $product_qty;

$inputData = [
    'qty' => $now_qty
];

DB::table('pha_pur_add_to_cart')
->where('id',$product_id)
->update($inputData);

}

$user = Auth::guard('user')->user()->username;

    $inputData = [
            'bill_id' => $nextno,
            'date' => $date,
            'date_time' => $date_time,
            'patient' => $request->patient_id,
            'pat_id' => $request->pat_id,
            'bed' => $request->bed,
            'total' => $total,
            'discount' => $request->discount,
            'in_total' => $total - $request->discount,
            'sales_user' => $user,
            'status' => $request->status,
        ];

    DB::table('pha_sales_manage')
    ->insert($inputData);

    if(!empty($request->pat_id)){

      $pha_sales_add_to_cart = DB::table('pha_sales_add_to_cart')
      ->where('bill_id',$nextno)
      ->get();

      

      foreach($pha_sales_add_to_cart as $value) {
        # code...

      $id = $value->id;

        $inputData = [
            'pat_id' => $request->pat_id,
        ];

        DB::table('pha_sales_add_to_cart')
        ->where('id',$id)
        ->update($inputData);

      }



    }

return \Redirect::route('Admin.phamedsalesbill', $nextno);

} else {

  $flashdata = ['class'=>'danger', 'message'=>"No Product Found for Sell "];
  return redirect()->back()->with($flashdata);

}

}


public function phamedsalesbill($pat_id)
{

// $data["bill_manage"] = DB::table('pha_sales_manage')
// ->where('bill_id',$id)
// ->first();

$data["pha_sales_add_to_cart"] = DB::table('pha_sales_add_to_cart')
->where('pat_id',$pat_id)
->get();

return Redirect()->route('Admin.medicinereturnbypatient');
}


public function phamedstock()
{

$data["current_stock"] = DB::table('pha_pur_add_to_cart')
->orderBy('id',"asc")
->get();

$data["phy_user"] = DB::table('users')
->where('pharmacy',1)
->orderBy('id',"asc")
->get();

return view('admin.pharmacyreport.phamedstock', $data);
}


public function phamedstockrep_med_wise(Request $request)
{

  $med_type = $request->medicine;

  if($med_type == "all"){

      $data["current_stock"] = DB::table('pha_pur_add_to_cart')
      ->orderBy('id',"asc")
      ->get();

  } else {
    $data["med_type"] = $med_type = $request->medicine;
    $data["current_stock"] = DB::table('pha_pur_add_to_cart')
    ->where('medicine_name',$med_type)
    ->get();

  }



return view('admin.pharmacyreport.phamedstockresult', $data);

}



public function phamedsales_all(Request $request)
{

  $sales_user = $request->sales_user;

  $data["from_date"] = $from_date = $request->from_date;
  $data["to_date"] = $to_date = $request->to_date;

  $data["formnewDate"] = $formnewDate = date("d/m/Y", strtotime($from_date));
  $data["tonewDate"] = $tonewDate = date("d/m/Y", strtotime($to_date));

  if($sales_user == "all"){

      $data["sales"] = DB::table('pha_sales_manage')
      ->whereBetween('date', [$formnewDate, $tonewDate])
      ->orderBy('id',"asc")
      ->get();

  } else {
    
    $data["sales_user"] = $sales_user = $request->sales_user;
    $data["sales"] = DB::table('pha_sales_manage')
      ->whereBetween('date', [$formnewDate, $tonewDate])
      ->where('sales_user',$sales_user)
      ->orderBy('id',"asc")
      ->get();

  }



return view('admin.pharmacyreport.phamedsales_all', $data);

}

public function medicinereturn()
{
  $data["bill_manage"] = DB::table('pha_sales_manage')
  ->orderBy('id','desc')
  ->get();

  return view('admin.pharmacysell.medicinereturn', $data);
}


public function medicinereturnbillview(Request $request)
{
  $id = $request->bil_id;

  return \Redirect::route('Admin.phamedreturnbill', $id);
}

public function medicinereturnbypatient()
{
  $data['patient_info'] = DB::table('patient_manage')
  ->orderBy('id','desc')
  ->get();

  return view('admin.pharmacysell.medicinereturnbypatient',$data);
}

public function medicinereturnbypatientbillview(REQUEST $request)
{
  $pat_id = $request->patient_id;

  // $medi_info = DB::table('pha_sales_add_to_cart')
  // ->where('pat_id',$pat_id)
  // ->get();

// dd($medi_info);
  
  
  return \Redirect::route('Admin.phamedreturnbillbypatient',$pat_id);
}

//alamin start
public function phamedreturnbillbypatient($pat_id)
{


  $data["pha_pur_add_to_cart"] = DB::table('pha_pur_add_to_cart')
  ->join('pha_medicine_purchase', 'pha_medicine_purchase.bill_id', '=', 'pha_pur_add_to_cart.bill_id')
  ->select('pha_pur_add_to_cart.*', 'pha_medicine_purchase.product_category')
  ->orderBy('id','desc')
  ->get();

    $data["patient_manage"] = DB::table('patient_manage')
        ->orderBy('id','desc')
        ->get();

    $data["bed_manage"] = DB::table('bed_manage')
        ->orderBy('id','desc')
        ->get();

  $nextno = $data["pat_id"] = $pat_id;
  

  $data["pha_sales_manage"] = DB::table('pha_sales_manage')
  ->where('pat_id', $nextno)
  ->first();

// dd($pat_id);

  $data['salesItem'] =$salesItem = DB::table('pha_sales_add_to_cart')
  ->where('pat_id', $nextno)
  ->orderBy('id', 'asc')
  ->get();

  return view('admin.pharmacysell.phamedsellreturnbypatient', $data);


}


public function return_adjust_patient(REQUEST $request)
{
  $bill_id = $request->bill_id;
  $pat_id = $request->pat_id;

foreach (array_combine($request->med_id, $request->qty) as $med_id => $qty) {

 $pha_sales_add_to_cart = $data["pha_sales_add_to_cart"] = DB::table('pha_sales_add_to_cart')
  ->where('id',$med_id)
  ->first();

  $price = $pha_sales_add_to_cart ->price;
  $cur_p_qty = $pha_sales_add_to_cart ->qty;
  $total_price = $price * $qty;


  $inputData = [
            'qty' => $qty,
            'total' => $total_price
        ];

    DB::table('pha_sales_add_to_cart')
    ->where('id',$med_id)
    ->where('pat_id',$pat_id)
    ->update($inputData); 

$med_name_new = $pha_sales_add_to_cart->med_name;

$iparr = preg_split("/[-]+/", $med_name_new); 
$pro_med = $iparr[1];

$pha_pur_add_to_cart = DB::table('pha_pur_add_to_cart')
->where('medicine_name', $pro_med)
->first();

$product_id = $pha_pur_add_to_cart->id;
$cur_stok_med = $pha_pur_add_to_cart->qty;


$now_qty = $cur_stok_med + $cur_p_qty;

$nqty = $now_qty - $qty;

$inputData = [
    'qty' => $nqty
];

DB::table('pha_pur_add_to_cart')
->where('id',$product_id)
->update($inputData);

}

$total_Sales = $data["pha_sales_add_to_cart"] = DB::table('pha_sales_add_to_cart')
  ->where('pat_id',$pat_id)
  ->sum('total');

$intotal_discount = DB::table('pha_sales_manage')
  ->where('pat_id',$pat_id)
  ->first();

$discount = $intotal_discount->discount;

$in_total = $total_Sales - $discount;


$inputData = [
    'total' => $total_Sales,
    'in_total' => $in_total
];


DB::table('pha_sales_manage')
->where('pat_id',$pat_id)
->update($inputData); 

return \Redirect::route('Admin.phamedsalesbill', $pat_id);

}
//End alamin


public function return_adjust(Request $request){

$bill_id = $request->bill_id;
foreach (array_combine($request->med_id, $request->qty) as $med_id => $qty) {

 $pha_sales_add_to_cart = $data["pha_sales_add_to_cart"] = DB::table('pha_sales_add_to_cart')
  ->where('id',$med_id)
  ->where('bill_id',$bill_id)
  ->first();

  $price = $pha_sales_add_to_cart ->price;
  $cur_p_qty = $pha_sales_add_to_cart ->qty;
  $total_price = $price * $qty;


  $inputData = [
            'qty' => $qty,
            'total' => $total_price
        ];

    DB::table('pha_sales_add_to_cart')
    ->where('id',$med_id)
    ->where('bill_id',$bill_id)
    ->update($inputData); 

$med_name_new = $pha_sales_add_to_cart->med_name;

$iparr = preg_split("/[-]+/", $med_name_new); 
$pro_med = $iparr[1];

$pha_pur_add_to_cart = DB::table('pha_pur_add_to_cart')
->where('medicine_name', $pro_med)
->first();

$product_id = $pha_pur_add_to_cart->id;
$cur_stok_med = $pha_pur_add_to_cart->qty;


$now_qty = $cur_stok_med + $cur_p_qty;

$nqty = $now_qty - $qty;

$inputData = [
    'qty' => $nqty
];

DB::table('pha_pur_add_to_cart')
->where('id',$product_id)
->update($inputData);

}

$total_Sales = $data["pha_sales_add_to_cart"] = DB::table('pha_sales_add_to_cart')
  ->where('bill_id',$bill_id)
  ->sum('total');

$intotal_discount = DB::table('pha_sales_manage')
  ->where('bill_id',$bill_id)
  ->first();

$discount = $intotal_discount->discount;

$in_total = $total_Sales - $discount;


$inputData = [
    'total' => $total_Sales,
    'in_total' => $in_total
];


DB::table('pha_sales_manage')
->where('bill_id',$bill_id)
->update($inputData); 


return \Redirect::route('Admin.phamedsalesbill', $bill_id);

}


public function phamedreturnbill($id){

  $data["pha_pur_add_to_cart"] = DB::table('pha_pur_add_to_cart')
  ->join('pha_medicine_purchase', 'pha_medicine_purchase.bill_id', '=', 'pha_pur_add_to_cart.bill_id')
  ->select('pha_pur_add_to_cart.*', 'pha_medicine_purchase.product_category')
  ->orderBy('id','desc')
  ->get();

    $data["patient_manage"] = DB::table('patient_manage')
        ->orderBy('id','desc')
        ->get();

    $data["bed_manage"] = DB::table('bed_manage')
        ->orderBy('id','desc')
        ->get();

$nextno = $data["bill_id"] = $id;

$data["pha_sales_manage"] = DB::table('pha_sales_manage')
->where('bill_id', $nextno)
->first();


$data['salesItem'] =$salesItem = DB::table('pha_sales_add_to_cart')
->where('bill_id', $nextno)
->orderBy('id', 'asc')
->get();

return view('admin.pharmacysell.phamedsellreturn', $data);

}




public function med_sales_return_delete_from_cart(Request $request)
    {
        $nextno = $request->bill_id;
        $med_id = $request->med_id;

       

$pha_sales_add_to_cart = DB::table('pha_sales_add_to_cart')
->where('id', $med_id)
->first();

$current_qty = $pha_sales_add_to_cart->qty;
$med_name_new = $pha_sales_add_to_cart->med_name;
$med_total = $pha_sales_add_to_cart->total;

$iparr = preg_split("/[-]+/", $med_name_new); 
$pro_med = $iparr[1];

$pha_pur_add_to_cart = DB::table('pha_pur_add_to_cart')
->where('medicine_name', $pro_med)
->first();

$product_id = $pha_pur_add_to_cart->id;
$cur_stok_med = $pha_pur_add_to_cart->qty;


$now_qty = $cur_stok_med + $current_qty;

$inputData = [
    'qty' => $now_qty
];

DB::table('pha_pur_add_to_cart')
->where('id',$product_id)
->update($inputData);

$pha_sales_manage = DB::table('pha_sales_manage')
->where('bill_id', $nextno)
->first();
$cur_total = $pha_sales_manage->in_total;

$now_total = $cur_total - $med_total;

$inputData = [
    'total' => $now_total,
    'in_total' => $now_total,
];

DB::table('pha_sales_manage')
->where('bill_id', $nextno)
->update($inputData);

 DB::table('pha_sales_add_to_cart')
        ->where('id', $med_id)
        ->where('bill_id', $nextno)
        ->delete();

$salesItem = DB::table('pha_sales_add_to_cart')
->where('bill_id', $nextno)
->orderBy('id', 'asc')
->get();

         $html = '<table class="table table-bordered scrollContent">
          <thead>
            <tr>
              <th style="width: 40%">Medicine</th>
              <th style="width: 10%">Qty</th>
              <th style="width: 15%">Amount </th>
              <th style="width: 15%">Total </th>
              <th style="width: 10%">Action</th>
            </tr>
          </thead> <tbody>';
          foreach ($salesItem as $key => $data) {
           $html.= '<tr>
                  <td>
              <input type="hidden" name="med_id" id="med_id" value="'.$data->id.'">
              <input type="hidden" name="bill_id" id="bill_id" value="'.$nextno.'">

                  '.$data->med_name.'</td>
                  <td>'.$data->qty.'</td>
                  <td>'.$data->price.'</td>
                  <td>'.$data->total.'</td>
                  <td><button type="button" class="btn btn-danger btn-sm waves-effect waves-light" onclick="deletefn()"><i class="fa fa-times"></i>
          </button></td>
                </tr>';
            }


           $html.= '</tbody>
</table>';

return $html;  

}


public function phapatbillreport()
{

$data["patient_manage"] = DB::table('patient_manage')
->orderBy('id',"asc")
->get();

return view('admin.pharmacyreport.phapatbillreport', $data);
}


public function phapatbillreportres(Request $request)
{

  $pat_id = $request->pat_id;

  $data["patient_manage"] = DB::table('patient_manage')
  ->where('id',$pat_id)
  ->first();

      $data["sales"] = DB::table('pha_sales_manage')
      ->where('pat_id', $pat_id)
      ->orderBy('id',"asc")
      ->get();

return view('admin.pharmacyreport.phapatbillreportres', $data);

}



//END
}
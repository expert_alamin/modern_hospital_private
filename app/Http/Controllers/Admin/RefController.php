<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Mode\User;
use App\Http\Controllers\Controller;
use App\Model\Logs;
use DB;

class RefController extends Controller
{
    
    public function refmanage()
    {

        $data["ref_manage"] = DB::table('ref_manage')
        ->orderBy('id','desc')
        ->get();

        // echo "<pre>";
        // print_r($data);
        // exit();

        return view('admin.refmanage.refmanage', $data);
    }

    
    public function refadd()
    {


        // echo "<pre>";
        // print_r($data);
        // exit();

        return view('admin.refmanage.refadd');
    }

    public function refaddac(Request $request)
    {

        $inputData = [
            'ref_code' => $request->ref_code,
            'ref_name' => $request->ref_name,
            'ref_phone' => $request->ref_phone,
            'ref_address' => $request->ref_address,
            'remarks' => $request->remarks,
        ];


        DB::table('ref_manage')
        ->insert($inputData);

        // echo "<pre>";
        // print_r($data);
        // exit();

        $flashdata = ['class'=>'success', 'message'=>"Add Successfull "];
        return redirect()->back()->with($flashdata);
    }

    
    public function refdelete($id)
    {

        DB::table('ref_manage')->where('id', $id)->delete();

        // echo "<pre>";
        // print_r($data);
        // exit();

        $flashdata = ['class'=>'danger', 'message'=>"Delete Successfull "];
        return redirect()->back()->with($flashdata);
    }

}
<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use App\Mode\User;
use App\Http\Controllers\Controller;
use App\Model\Logs;
use DB;
use Auth;



class LabController extends Controller
{
    
    public function testlist ()
    {

        $data["testlist"] = DB::table('bill_add_to_cart')
        ->join('bill_manage', 'bill_manage.bill_id', '=', 'bill_add_to_cart.bill_id')
        ->join('patient_manage', 'patient_manage.id', '=', 'bill_manage.patient_id')
        ->select('bill_add_to_cart.*', 
            'patient_manage.pat_type',
            'patient_manage.pat_id',
            'patient_manage.name',
            'patient_manage.age',
            'patient_manage.gender',
            'patient_manage.phone',
            'patient_manage.address',
            'patient_manage.gur_name',
            'patient_manage.remarks',
            'patient_manage.address'
          )
        ->where('bill_add_to_cart.report_status',null)
        ->wherenull('bill_add_to_cart.report_result')
        ->orderBy('bill_add_to_cart.id','desc')
        ->get();

        // echo "<pre>";
        // print_r($data);
        // exit();

        return view('admin.lab.testlist', $data);
    }

    
    public function testlistcomplete ()
    {

        $data["testlist"] = DB::table('bill_add_to_cart')
        ->join('bill_manage', 'bill_manage.bill_id', '=', 'bill_add_to_cart.bill_id')
        ->join('patient_manage', 'patient_manage.id', '=', 'bill_manage.patient_id')
        ->select('bill_add_to_cart.*', 
            'patient_manage.pat_type',
            'patient_manage.pat_id',
            'patient_manage.name',
            'patient_manage.age',
            'patient_manage.gender',
            'patient_manage.phone',
            'patient_manage.address',
            'patient_manage.gur_name',
            'patient_manage.remarks',
            'patient_manage.address'
          )
        ->where('bill_add_to_cart.c_type',1)
        ->wherenotnull('bill_add_to_cart.report_result')
        ->orderBy('bill_add_to_cart.id','desc')
        ->get();

        // echo "<pre>";
        // print_r($data);
        // exit();

        return view('admin.lab.testlist', $data);
    }
    
    public function testressub($id)
    {

        $from_add_to_cart=DB::table('bill_add_to_cart')->where('bill_add_to_cart.id',$id)->first();

        $for_id=$from_add_to_cart->charge_name;

        $iparr = preg_split("/[-]+/", $for_id);
        $pro_med_id=$iparr[0]; 
        $pro_med_name = $iparr[1];

        $data["investigation_data"]=DB::table('investigation_manage')->where('code_test',$pro_med_id)->first();
        


       $data["data"] = DB::table('bill_add_to_cart')
        ->join('bill_manage', 'bill_manage.bill_id', '=', 'bill_add_to_cart.bill_id')
        ->join('patient_manage', 'patient_manage.id', '=', 'bill_manage.patient_id')
        ->select('bill_add_to_cart.*', 
            'patient_manage.pat_type',
            'patient_manage.pat_id',
            'patient_manage.name',
            'patient_manage.age',
            'patient_manage.gender',
            'patient_manage.phone',
            'patient_manage.address',
            'patient_manage.gur_name',
            'patient_manage.remarks',
            'patient_manage.address'
          )
        ->where('bill_add_to_cart.c_type',1)
        ->where('bill_add_to_cart.id',$id)
        ->first();

        // echo "<pre>";
        // print_r($data);
        // exit();

       return view('admin.lab.testlistressub',$data);
    }

    

    

    public function testressubac(Request $request)
    {

        date_default_timezone_set('Asia/Dhaka');
        $date = date("d/m/Y");
        $date_time = date("d/m/Y h:i:s a");

        $inputData = [
                'report_time' => $date_time,
                'report_result' => $request->report_result,
                'report_status' => "Submitted"
        ];


        DB::table('bill_add_to_cart')
        ->where('id',$request->id)
        ->update($inputData);

        // echo "<pre>";
        // print_r($data);
        // exit();

        return \Redirect::route('Admin.testresprint', $request->id);

      }

      
    public function testresprint($id)
    {
        
        $from_add_to_cart=DB::table('bill_add_to_cart')->where('bill_add_to_cart.id',$id)->first();
        $for_id=$from_add_to_cart->charge_name;

        $iparr = preg_split("/[-]+/", $for_id);
        $pro_med_id=$iparr[0]; 
        $pro_med_name = $iparr[1];
        $data["addcart_data"]=$from_add_to_cart;
        
         $data["investigation_data"]=DB::table('investigation_manage')->where('code_test',$pro_med_id)->first();
      

        $data["bill_manage"] = DB::table('bill_add_to_cart')
        ->join('bill_manage', 'bill_manage.bill_id', '=', 'bill_add_to_cart.bill_id')
        ->join('patient_manage', 'patient_manage.id', '=', 'bill_manage.patient_id')
        
        ->select('bill_add_to_cart.*', 
            'patient_manage.pat_type',
            'patient_manage.pat_id',
            'patient_manage.name',
            'patient_manage.age',
            'patient_manage.gender',
            'patient_manage.phone',
            'patient_manage.address',
            'patient_manage.gur_name',
            'patient_manage.remarks',
            'patient_manage.address'
          )
        ->where('bill_add_to_cart.c_type',1)
        ->where('bill_add_to_cart.id',$id)
        ->first();

        // echo "<pre>";
        // print_r($data);
        // exit();

        return view('admin.lab.testresprint', $data);
    }

    public function testlistprint(){
         $data["testlistprint"] = DB::table('bill_add_to_cart')
        ->join('bill_manage', 'bill_manage.bill_id', '=', 'bill_add_to_cart.bill_id')
        ->join('patient_manage', 'patient_manage.id', '=', 'bill_manage.patient_id')
        ->select('bill_add_to_cart.*', 
            'patient_manage.name'
            
          )
        ->where('bill_add_to_cart.c_type',1)
        ->wherenotnull('bill_add_to_cart.report_result')
        ->orderBy('bill_add_to_cart.id','desc')
        ->get();
        return view('admin.lab.testlistprint',$data);


    }
    public function testalllistresprint($id){

        $from_add_to_cart=DB::table('bill_add_to_cart')->where('bill_add_to_cart.bill_id',$id)->get();
        
        
        foreach ($from_add_to_cart as $key ) {
            $for_id =$key->charge_name;
            $iparr = preg_split("/[-]+/", $for_id);
            $pro_med_id=$iparr[0]; 
            $pro_med_name = $iparr[1];
           

            $data["addcart_data"]=$from_add_to_cart;

         $data["investigation_data"]=DB::table('investigation_manage')->where('code_test',$pro_med_id)->get();
        }

        
        
      

        $data["bill_manage"] = DB::table('bill_add_to_cart')
        ->join('bill_manage', 'bill_manage.bill_id', '=', 'bill_add_to_cart.bill_id')
        ->join('patient_manage', 'patient_manage.id', '=', 'bill_manage.patient_id')
        
        ->select('bill_add_to_cart.*', 
            'patient_manage.pat_type',
            'patient_manage.pat_id',
            'patient_manage.name',
            'patient_manage.age',
            'patient_manage.gender',
            'patient_manage.phone',
            'patient_manage.address',
            'patient_manage.gur_name',
            'patient_manage.remarks',
            'patient_manage.address'
          )
        ->where('bill_add_to_cart.c_type',1)
        ->where('bill_add_to_cart.id',$id)
        ->first();

        // echo "<pre>";
        // print_r($data);
        // exit();

        return view('admin.lab.testresprint', $data);


    }

//END
}
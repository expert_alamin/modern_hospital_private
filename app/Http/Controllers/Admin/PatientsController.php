<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Mode\User;
use App\Http\Controllers\Controller;
use App\Model\Logs;
use DB;

class PatientsController extends Controller
{
    
    public function patients()
    {

        $data["patient_manage"] = DB::table('patient_manage')
        ->orderBy('id','desc')
        ->get();

        // echo "<pre>";
        // print_r($data);
        // exit();

        return view('admin.patients.patients', $data);
    }

    public function opd()
    {

        $data["patient_manage"] = DB::table('patient_manage')
        ->where('pat_type','OPD - Out Patient')
        ->orderBy('id','desc')
        ->get();

        // echo "<pre>";
        // print_r($data);
        // exit();

        return view('admin.patients.opd', $data);
    }

    public function ipd()
    {

        $data["patient_manage"] = DB::table('patient_manage')
        ->where('pat_type','IPD - In Patient')
        ->orderBy('id','desc')
        ->get();

        // echo "<pre>";
        // print_r($data);
        // exit();

        return view('admin.patients.ipd', $data);
    }

    
    public function patientsadd()
    {


        // echo "<pre>";
        // print_r($data);
        // exit();

        return view('admin.patients.patientsadd');
    }

    public function patientsaddac(Request $request)
    {

        $pat_info = DB::table('patient_manage')
        ->orderBy('id','desc')
        ->first();

        @$pat_id_get = $pat_info->id;
        if(empty($pat_id_get)){
            $pat_id = 1;
        } else{
            $pat_id = $pat_id = $pat_info->id+1;
        }

        $inputData = [
            'pat_id' => $pat_id,
            'pat_type' => $request->pat_type,
            'name' => $request->name,
            'age' => $request->age,
            'gender' => $request->gender,
            'phone' => $request->phone,
            'address' => $request->address,
            'gur_name' => $request->gur_name,
            'remarks' => $request->remarks,
        ];

        DB::table('patient_manage')
        ->insert($inputData);

        // echo "<pre>";
        // print_r($data);
        // exit();

        $flashdata = ['class'=>'success', 'message'=>"Add Successfull "];
        return redirect()->back()->with($flashdata);
    }

    
    public function patientsdelete($id)
    {

        DB::table('patient_manage')->where('id', $id)->delete();

        // echo "<pre>";
        // print_r($data);
        // exit();

        $flashdata = ['class'=>'danger', 'message'=>"Delete Successfull "];
        return redirect()->back()->with($flashdata);
    }

}
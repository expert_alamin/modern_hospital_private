<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

use App\Mode\User;
use App\Http\Controllers\Controller;
use App\Model\Logs;
use DB;
use Auth;


class OfficeConroller extends Controller
{
    
    public function officebilllist()
    {

        $data["bill_manage"] = DB::table('bill_manage')
        ->orderBy('id','desc')
        ->get();

        // echo "<pre>";
        // print_r($data);
        // exit();

        return view('admin.office.officebilllist', $data);
    }

    public function officebilllistquery(Request $request)
    {

        date_default_timezone_set('Asia/Dhaka');
        $date = date("d/m/Y");
        $date_time = date("d/m/Y h:i:s a");  

        $data["from_date"] = $from_date = $request->from_date;
        $data["to_date"] = $to_date = $request->to_date;

        $data["from_date_new"] = $formnewDate = date("d/m/Y", strtotime($from_date));
        $data["to_date_new"] = $tonewDate = date("d/m/Y", strtotime($to_date));

        $data["bill_manage"] = DB::table('bill_manage')
        ->whereBetween('date', [$formnewDate, $tonewDate])
        ->orderBy('id','desc')
        ->get();


        // echo "<pre>";
        // print_r($data);
        // exit();

        return view('admin.office.officebilllistquery', $data);
    }


 public function officerfbilllist()
    {

        $data["bill_manage"] = DB::table('bill_manage')
        ->wherenotnull('dr_id')
        ->orderBy('id','desc')
        ->get();

        // echo "<pre>";
        // print_r($data);
        // exit();

        return view('admin.office.officerfbilllist', $data);
    }

public function rfbillpayac(Request $request)
    {
      date_default_timezone_set('Asia/Dhaka');
        $date = date("d/m/Y");
        $date_time = date("d/m/Y h:i:s a");  


        $id = $request->id;

        $amountrf = $request->amountrf;

        $inputData = [
            'rf_bill_status' => "Paid",
            'rf_bill_date_time' => $date_time,
            'amountrf' => $amountrf,
        ];

        DB::table('bill_manage')
        ->where('id',$id)
        ->update($inputData);

        return redirect()->back();
    }

   
public function rfbillprint($id)
{

$data["bill_manage"] = DB::table('bill_manage')
->join('patient_manage', 'patient_manage.id', '=', 'bill_manage.patient_id')
->leftjoin('doctor_manage', 'doctor_manage.id', '=', 'bill_manage.dr_id')
->leftjoin('bed_manage', 'bed_manage.id', '=', 'bill_manage.bed_id')
->select('bill_manage.*', 
  'patient_manage.pat_type',
  'patient_manage.pat_id',
  'patient_manage.name',
  'patient_manage.age',
  'patient_manage.gender',
  'patient_manage.phone',
  'patient_manage.address',
  'patient_manage.gur_name',
  'patient_manage.remarks',
  'patient_manage.address',
  'doctor_manage.dr_code',
  'doctor_manage.dr_name',
  'doctor_manage.dr_phone',
  'bed_manage.bed_name',
  'bed_manage.bed_type',
  'bed_manage.floor'
)
->where('bill_manage.bill_id',$id)
->first();

// echo "<pre>";
// print_r($data);
// exit()

$data["bill_add_to_cart"] = DB::table('bill_add_to_cart')
->where('bill_add_to_cart.bill_id',$id)
->get();

return view('admin.office.rfbillprint', $data);
} 

}
<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Mode\User;
use App\Http\Controllers\Controller;
use App\Model\Logs;
use DB;

class BedController extends Controller
{
    
    public function bedmanage()
    {

        $data["bed_manage"] = DB::table('bed_manage')
        ->orderBy('id','desc')
        ->get();

        // echo "<pre>";
        // print_r($data);
        // exit();

        return view('admin.bed.bed_manage', $data);
    }

    
    public function bedadd()
    {

        $data["bed_type"] = DB::table('bed_type')
        ->orderBy('id','asc')
        ->get();

        $data["bed_floor"] = DB::table('bed_floor')
        ->orderBy('id','asc')
        ->get();

        // echo "<pre>";
        // print_r($data);
        // exit();

        return view('admin.bed.bedadd', $data);
    }

    public function bedaddac(Request $request)
    {

        $inputData = [
            'bed_name' => $request->bed_name,
            'bed_type' => $request->bed_type,
            'floor' => $request->floor,
            'status' => "Active",
            
        ];


        DB::table('bed_manage')
        ->insert($inputData);

        // echo "<pre>";
        // print_r($data);
        // exit();

        $flashdata = ['class'=>'success', 'message'=>"Bed Add Successfull "];
        return redirect()->back()->with($flashdata);
    }

    
    public function beddelete($id)
    {

        DB::table('bed_manage')->where('id', $id)->delete();

        // echo "<pre>";
        // print_r($data);
        // exit();

        $flashdata = ['class'=>'danger', 'message'=>"Bed Delete Successfull "];
        return redirect()->back()->with($flashdata);
    }

}
<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Mode\User;
use App\Http\Controllers\Controller;
use App\Model\Logs;
use DB;

class DoctorsController extends Controller
{
    
    public function doctors()
    {

        $data["doctor_manage"] = DB::table('doctor_manage')
        ->orderBy('id','desc')
        ->get();

        // echo "<pre>";
        // print_r($data);
        // exit();

        return view('admin.doctors.doctors', $data);
    }

    
    public function doctorsadd()
    {

        $data["ref_manage"] = DB::table('ref_manage')
        ->orderBy('id','desc')
        ->get();

        // echo "<pre>";
        // print_r($data);
        // exit();

        return view('admin.doctors.doctorsadd',$data);
    }

    public function doctorsaddac(Request $request)
    {

       $ref_manage = $data["ref_manage"] = DB::table('ref_manage')
        ->where('id',$request->mr_id)
        ->first();

        $mr_id = $ref_manage->ref_code;
        $mr_name = $ref_manage->ref_name;

        $inputData = [
            'dr_code' => $request->dr_code,
            'dr_name' => $request->dr_name,
            'dr_phone' => $request->dr_phone,
            'dr_address' => $request->dr_address,
            'remarks' => $request->remarks,
            'mr_id' => $mr_id,
            'mr_name' => $mr_name,
            'mr_com' => $request->mr_com,
        ];


        DB::table('doctor_manage')
        ->insert($inputData);

        // echo "<pre>";
        // print_r($data);
        // exit();

        $flashdata = ['class'=>'success', 'message'=>"Add Successfull "];
        return redirect()->back()->with($flashdata);
    }

    
    public function doctorsdelete($id)
    {

        DB::table('doctor_manage')->where('id', $id)->delete();

        // echo "<pre>";
        // print_r($data);
        // exit();

        $flashdata = ['class'=>'danger', 'message'=>"Delete Successfull "];
        return redirect()->back()->with($flashdata);
    }

}
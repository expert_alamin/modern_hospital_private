<?php
URL::forceScheme('https');
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Wizard
Route::get('install', 'Expert\WizardController@install')->name('install');
Route::post('db-connection', 'Expert\WizardController@dbConnection')->name('dbConnection');
Route::post('wizard-action', 'Expert\WizardController@wizardAction')->name('wizardAction');

//Work-Start
Route::group(['middleware'=>'dbWizard'], function () {

    Route::get('login', 'Expert\HomeController@login')->name('login');
    Route::post('login', 'Expert\HomeController@loginAction')->name('login');
    Route::get('logout', 'Expert\HomeController@logout')->name('logout');

    Route::group(['middleware'=>'userAuth'], function () {
    Route::get('/', 'Expert\HomeController@app')->name('home');

    	//Admin
    	Route::group(['as'=>'Admin.', 'prefix'=>'admin', 'namespace'=>'Admin'], function () {
        	Route::get('dashboard', 'HomeController@dashboard')->name('dashboard');
        	Route::resource('userrole', 'UserroleController');
        	Route::resource('usermanage', 'UserController');
        	Route::get('systemlogs', 'HomeController@systemlogs')->name('systemlogs');
        	Route::get('settings', 'HomeController@settings')->name('settings');
        	Route::post('settingsupdate', 'HomeController@settingsupdate')->name('settingsupdate');

            Route::get('currency', 'HomeController@currency')->name('currency');
            Route::get('showcursymbol', 'HomeController@showcursymbol')->name('showcursymbol');
            Route::get('showcurtext', 'HomeController@showcurtext')->name('showcurtext');
            Route::post('currencyupdate', 'HomeController@currencyupdate')->name('currencyupdate');
                
                
            Route::get('profileupdate', 'UpdateUserController@profileupdate')->name('profileupdate');
            Route::post('updateuser', 'UpdateUserController@updateuser')->name('updateuser');

            Route::get('changepassword', 'UpdateUserController@changepassword')->name('changepassword');
            Route::post('updateuserpass', 'UpdateUserController@updateuserpass')->name('updateuserpass');

            

        // Backup routes
            Route::get('backup', 'BackupController@index');
            Route::get('backup/create', 'BackupController@create');
            Route::get('backup/backup-download/{file_name}', 'BackupController@download');
            Route::get('backup/delete/{file_name}', 'BackupController@delete');
            


            Route::get('bedmanage', 'BedController@bedmanage')->name('bedmanage');
            Route::get('bedadd', 'BedController@bedadd')->name('bedadd');
            Route::post('bedaddac', 'BedController@bedaddac')->name('bedaddac');
            Route::get('beddelete/{id}', 'BedController@beddelete')->name('beddelete');

            Route::get('hospitalcharge', 'HospitalChargeController@hospitalcharge')->name('hospitalcharge');
            Route::get('hospitalchargeadd', 'HospitalChargeController@hospitalchargeadd')->name('hospitalchargeadd');
            Route::post('hospitalchargeac', 'HospitalChargeController@hospitalchargeac')->name('hospitalchargeac');
            Route::get('hospitalchargedelete/{id}', 'HospitalChargeController@hospitalchargedelete')->name('hospitalchargedelete');

            
            Route::get('investigations', 'InvestigationController@investigations')->name('investigations');
            Route::get('investigationsadd', 'InvestigationController@investigationsadd')->name('investigationsadd');
            Route::post('investigationsac', 'InvestigationController@investigationsac')->name('investigationsac');
            Route::get('investigationsdelete/{id}', 'InvestigationController@investigationsdelete')->name('investigationsdelete');
            
            Route::get('invedit/{id}', 'InvestigationController@invedit')->name('invedit');
            Route::post('inveditac/{id}', 'InvestigationController@inveditac')->name('inveditac');

            Route::get('doctors', 'DoctorsController@doctors')->name('doctors');
            Route::get('doctorsadd', 'DoctorsController@doctorsadd')->name('doctorsadd');
            Route::post('doctorsaddac', 'DoctorsController@doctorsaddac')->name('doctorsaddac');
            Route::get('doctorsdelete/{id}', 'DoctorsController@doctorsdelete')->name('doctorsdelete');
            
            
            Route::get('refmanage', 'RefController@refmanage')->name('refmanage');
            Route::get('refadd', 'RefController@refadd')->name('refadd');
            Route::post('refaddac', 'RefController@refaddac')->name('refaddac');
            Route::get('refdelete/{id}', 'RefController@refdelete')->name('refdelete');
            
            Route::get('opd', 'PatientsController@opd')->name('opd');
            Route::get('ipd', 'PatientsController@ipd')->name('ipd');

            Route::get('patients', 'PatientsController@patients')->name('patients');
            Route::get('patientsadd', 'PatientsController@patientsadd')->name('patientsadd');
            Route::post('patientsaddac', 'PatientsController@patientsaddac')->name('patientsaddac');
            Route::get('patientsdelete/{id}', 'PatientsController@patientsdelete')->name('patientsdelete');

            Route::get('bill', 'BillController@bill')->name('bill');
            Route::get('billadd', 'BillController@billadd')->name('billadd');
            Route::get('charge_show', 'BillController@charge_show')->name('charge_show');
            Route::get('investigation_show', 'BillController@investigation_show')->name('investigation_show');
            Route::get('add_item_to_cart', 'BillController@add_item_to_cart')->name('add_item_to_cart');
            Route::get('delete_from_cart', 'BillController@delete_from_cart')->name('delete_from_cart');
            Route::get('calculate_total', 'BillController@calculate_total')->name('calculate_total');

            
            Route::get('hc_rf_calculate_total', 'BillController@hc_rf_calculate_total')->name('hc_rf_calculate_total');

            Route::post('billaction', 'BillController@billaction')->name('billaction');
            Route::get('billprint/{id}', 'BillController@billprint')->name('billprint');
            
            Route::get('billpayment/{id}', 'BillController@billpayment')->name('billpayment');
            Route::post('billpaymentaction/{id}', 'BillController@billpaymentaction')->name('billpaymentaction');
            
            //Pharmacy 

            Route::get('phaproductcategory', 'PharmacyConroller@phaproductcategory')->name('phaproductcategory');
            Route::post('phaproductcategoryadd', 'PharmacyConroller@phaproductcategoryadd')->name('phaproductcategoryadd');

            
            Route::get('pharcatedit/{id}', 'PharmacyConroller@pharcatedit')->name('pharcatedit');
            Route::post('phaproductcategoryeditac/{id}', 'PharmacyConroller@phaproductcategoryeditac')->name('phaproductcategoryeditac');
            

            Route::get('pharcatdel/{id}', 'PharmacyConroller@pharcatdel')->name('pharcatdel');

            Route::get('phamedicinecompany', 'PharmacyConroller@phamedicinecompany')->name('phamedicinecompany');
            Route::post('phamedicinecompanyadd', 'PharmacyConroller@phamedicinecompanyadd')->name('phamedicinecompanyadd');
            Route::get('phamedicinecompanydelete/{id}', 'PharmacyConroller@phamedicinecompanydelete')->name('phamedicinecompanydelete');

            
            Route::get('phamedpurlist', 'PharmacyConroller@phamedpurlist')->name('phamedpurlist');
            Route::get('phamedpur', 'PharmacyConroller@phamedpur')->name('phamedpur');
            Route::get('pha_pur_med_add_item_to_cart', 'PharmacyConroller@pha_pur_med_add_item_to_cart')->name('pha_pur_med_add_item_to_cart');
            Route::get('pha_pur_med_delete_from_cart', 'PharmacyConroller@pha_pur_med_delete_from_cart')->name('pha_pur_med_delete_from_cart');
            Route::post('phamedpuraction', 'PharmacyConroller@phamedpuraction')->name('phamedpuraction');
            Route::get('phamedpurbill/{id}', 'PharmacyConroller@phamedpurbill')->name('phamedpurbill');

            
            Route::get('phamedpurbilledit/{id}', 'PharmacyConroller@phamedpurbilledit')->name('phamedpurbilledit');
            Route::post('phamedpureditac/{id}', 'PharmacyConroller@phamedpureditac')->name('phamedpureditac');
            
            
            Route::get('phamedselllist', 'PharmacyConroller@phamedselllist')->name('phamedselllist');
            Route::get('phamedsell', 'PharmacyConroller@phamedsell')->name('phamedsell');

            
            Route::get('medicinesellshow', 'PharmacyConroller@medicinesellshow')->name('medicinesellshow');
            
            Route::get('med_sales_add_item_to_cart', 'PharmacyConroller@med_sales_add_item_to_cart')->name('med_sales_add_item_to_cart');
            Route::get('med_sales_delete_from_cart', 'PharmacyConroller@med_sales_delete_from_cart')->name('med_sales_delete_from_cart');
            
            
            Route::post('phamedsalesaction', 'PharmacyConroller@phamedsalesaction')->name('phamedsalesaction');
            Route::get('phamedsalesbill/{pat_id}', 'PharmacyConroller@phamedsalesbill')->name('phamedsalesbill');



            Route::get('medicinereturn', 'PharmacyConroller@medicinereturn')->name('medicinereturn');
            Route::post('medicinereturnbillview', 'PharmacyConroller@medicinereturnbillview')->name('medicinereturnbillview');
            Route::get('phamedreturnbill/{id}', 'PharmacyConroller@phamedreturnbill')->name('phamedreturnbill');
            
            //alamin start
            Route::get('medicinereturnbypatient', 'PharmacyConroller@medicinereturnbypatient')->name('medicinereturnbypatient');
            Route::post('medicinereturnbypatientbillview', 'PharmacyConroller@medicinereturnbypatientbillview')->name('medicinereturnbypatientbillview');    

            Route::get('phamedreturnbillbypatient/{pat_id}', 'PharmacyConroller@phamedreturnbillbypatient')->name('phamedreturnbillbypatient');

            Route::post('return_adjust_patient', 'PharmacyConroller@return_adjust_patient')->name('return_adjust_patient');

            //alamin end
           

            Route::get('med_sales_return_delete_from_cart', 'PharmacyConroller@med_sales_return_delete_from_cart')->name('med_sales_return_delete_from_cart');

            Route::get('phamedstock', 'PharmacyConroller@phamedstock')->name('phamedstock');
            Route::post('phamedstockrep_med_wise', 'PharmacyConroller@phamedstockrep_med_wise')->name('phamedstockrep_med_wise');

            Route::post('phamedsales_all', 'PharmacyConroller@phamedsales_all')->name('phamedsales_all');
            
            Route::post('return_adjust', 'PharmacyConroller@return_adjust')->name('return_adjust');

            
            Route::get('phapatbillreport', 'PharmacyConroller@phapatbillreport')->name('phapatbillreport');
            
            Route::post('phapatbillreportres', 'PharmacyConroller@phapatbillreportres')->name('phapatbillreportres');

            //OFFICE
            Route::get('officebilllist', 'OfficeConroller@officebilllist')->name('officebilllist');
            Route::post('officebilllistquery', 'OfficeConroller@officebilllistquery')->name('officebilllistquery');

            Route::get('officerfbilllist', 'OfficeConroller@officerfbilllist')->name('officerfbilllist');
            Route::post('rfbillpayac/{id}', 'OfficeConroller@rfbillpayac')->name('rfbillpayac');
            Route::get('rfbillprint/{id}', 'OfficeConroller@rfbillprint')->name('rfbillprint');
            


            
            //Lab 
            
            Route::get('testlist', 'LabController@testlist')->name('testlist');
            Route::get('testressub/{id}', 'LabController@testressub')->name('testressub');
            Route::post('testressubac/{id}', 'LabController@testressubac')->name('testressubac');
            Route::get('testresprint/{id}', 'LabController@testresprint')->name('testresprint');

            
            Route::get('testlistcomplete', 'LabController@testlistcomplete')->name('testlistcomplete');

            Route::get('testlistprint', 'LabController@testlistprint')->name('testlistprint');

            Route::get('testalllistresprint/{id}', 'LabController@testalllistresprint')->name('testalllistresprint');
            
            
    	});


    });

});
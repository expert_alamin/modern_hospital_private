@extends('expert.master')

@section('title', 'Medicine Purchase List Phaymacy - '.$settingsinfo->company_name.' - '.$settingsinfo->soft_name.'')

@section('content')

@include('expert.sidebar')

@include('expert.topbar')

<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
      <div class="row">

        <?php if (session('message')): ?>
          <div class="col-lg-12">
              <div class="alert alert-{{session('class')}} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <div class="alert-icon contrast-alert"><i class="icon-close"></i></div>
                <div class="alert-message"><span>{{session('message')}}</span></div>
              </div>
            </div>
        <?php endif; ?>

        <div class="col-lg-12">

      

          <div class="card bg-dark">
      		<div class="card-header border-0 bg-transparent text-white">
                <i class="fa fa-user-circle"></i><span>Medicine Purchase List Manage</span>
            </div>

            <div class="card">
            <div class="card-header">

              <div style="display:inline-block; padding-top:5px;">
                <i class="fa fa-table"></i> Medicine Purchase List
              </div> 

             

            </div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="dataTable" class="table table-bordered">
                <thead>
                    <tr>
                        <th width="5%">SN</th>
                        <th>Bill ID </th>
                        <th>Invoice No</th>
                        <th>Date </th>
                        <th>Supplier</th>
                        <th>Category</th>
                        <th>Company </th>
                        <th>Total </th>
                        <th width="8%" class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i=1; @endphp
                    @foreach($pha_medicine_purchase as $data)
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{$data->bill_id}}</td>
                        <td>{{$data->inv_num}}</td>
                        <td>{{$data->date}}</td>
                        <td>{{$data->supplier_name}} {{$data->supplier_phone}}</td>
                        <td>{{$data->product_category}}</td>
                        <td>{{$data->company_name}}</td>
                        <td>{{$data->total}}</td>
                        <td>

                          <a href="{{url('admin/phamedpurbilledit',$data->bill_id)}}" class="btn btn-warning btn-sm waves-effect waves-light"> 
                            <i class="fa fa-edit"></i> <span> Edit </span>
                          </a>

                          <a href="{{url('admin/phamedpurbill',$data->bill_id)}}" class="btn btn-success btn-sm waves-effect waves-light"> 
                            <i class="fa fa-print"></i> <span> Print Bill</span>
                          </a>

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
            </div>
          </div>
               
          </div>
        </div>



        



      </div><!--End Row-->
	  
       <!--End Dashboard Content-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   

  @include('expert.copyright')

  @endsection

  @section('js')
    <script>
    $(document).ready(function() {
        dataTableLoad({
            curUrl: "{{route('Admin.userrole.index')}}",
            addUrl: "{{route('Admin.userrole.create')}}"
        });
    });
    </script>
  @endsection
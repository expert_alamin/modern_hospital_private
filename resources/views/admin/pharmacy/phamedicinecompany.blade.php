@extends('expert.master')

@section('title', 'Medicine Company Phaymacy - '.$settingsinfo->company_name.' - '.$settingsinfo->soft_name.'')

@section('content')

@include('expert.sidebar')

@include('expert.topbar')

<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
      <div class="row">

        <?php if (session('message')): ?>
          <div class="col-lg-12">
              <div class="alert alert-{{session('class')}} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <div class="alert-icon contrast-alert"><i class="icon-close"></i></div>
                <div class="alert-message"><span>{{session('message')}}</span></div>
              </div>
            </div>
        <?php endif; ?>

        <div class="col-lg-8">

      

          <div class="card bg-dark">
      		<div class="card-header border-0 bg-transparent text-white">
                <i class="fa fa-user-circle"></i><span> Medicine Company Manage</span>
            </div>

            <div class="card">
            <div class="card-header">

              <div style="display:inline-block; padding-top:5px;">
                <i class="fa fa-table"></i> Medicine Company List
              </div> 

             

            </div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="dataTable" class="table table-bordered">
                <thead>
                    <tr>
                        <th width="5%">SN</th>
                        <th>Company Name</th>
                        <th width="8%" class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i=1; @endphp
                    @foreach($pha_medicine_comapany as $data)
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{$data->company_name}}</td>
                        <td>

                          <!-- <button type="button" class="btn btn-warning btn-sm waves-effect waves-light edit" data="{{$data->id}}"> <i class="fa fa-edit"></i> <span></span></button> --> 

                          <a href="{{url('admin/phamedicinecompanydelete',$data->id)}}" class="btn btn-danger btn-sm waves-effect waves-light"> 
                            <i class="fa fa-times"></i> <span></span>
                          </a>

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
            </div>
          </div>
               
          </div>
        </div>



        <div class="col-lg-4">

          

          <div class="card bg-dark">
          <div class="card-header border-0 bg-transparent text-white">
                <i class="fa fa-user-circle"></i><span> Medicine Company Add</span>
            </div>

            <div class="card">
            <div class="card-header">

              <div style="display:inline-block; padding-top:5px;">
                <i class="fa fa-table"></i> New Medicine Company
              </div> 

            </div>
            <div class="card-body">
             
              <form action="{{url('admin/phamedicinecompanyadd')}}" id="qcat" method="post">
              @csrf

              <div class="row">

                  <div class="col-md-12">
                      <div class="form-group">
                          <label for="name">Medicine Company Name </label>
                          <input required="" type="text" class="form-control" id="company_name" name="company_name" placeholder="Enter Medicine Company Name">
                      </div>
                  </div>


                  

                  <div class="col-md-12">
                  </div>
                  
                  

                  <div class="col-md-12">
                    <button type="submit" class="btn btn-dark btn-block col-md-offset-2">
                      <i class="fa fa-check-square-o"></i> Save
                    </button>
                  </div>

              </div>

            </form>

            </div>
          </div>
               
          </div>
        </div>




      </div><!--End Row-->
	  
       <!--End Dashboard Content-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   

  @include('expert.copyright')

  @endsection

  @section('js')
    <script>
    $(document).ready(function() {
        dataTableLoad({
            curUrl: "{{route('Admin.userrole.index')}}",
            addUrl: "{{route('Admin.userrole.create')}}"
        });
    });
    </script>
  @endsection
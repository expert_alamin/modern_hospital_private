@extends('expert.master')

@section('title', 'Medicine Phaymacy - '.$settingsinfo->company_name.' - '.$settingsinfo->soft_name.'')

@section('content')

@include('expert.sidebar')

@include('expert.topbar')

<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
      <div class="row">

        <?php if (session('message')): ?>
          <div class="col-lg-12">
              <div class="alert alert-{{session('class')}} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <div class="alert-icon contrast-alert"><i class="icon-close"></i></div>
                <div class="alert-message"><span>{{session('message')}}</span></div>
              </div>
            </div>
        <?php endif; ?>

        

        <div class="col-lg-6">

          

          <div class="card bg-dark">
          <div class="card-header border-0 bg-transparent text-white">
                <i class="fa fa-user-circle"></i><span> Medicine Category Edit</span>
            </div>

            <div class="card">
            <div class="card-header">

              <div style="display:inline-block; padding-top:5px;">
                <i class="fa fa-table"></i> Edit Medicine Category
              </div> 

            </div>
            <div class="card-body">
             
              <form action="{{url('admin/phaproductcategoryeditac',$pha_category->id)}}" id="qcat" method="post">
              @csrf

              <div class="row">

                  <div class="col-md-12">
                      <div class="form-group">
                          <label for="name">Medicine Category Name </label>
                          <input required="" type="text" class="form-control" id="category_name" name="category_name" placeholder="Enter Medicine Category Name" value="{{$pha_category->category_name}}">
                      </div>
                  </div>


                  

                  <div class="col-md-12">
                  </div>
                  
                  

                  <div class="col-md-12">
                    <button type="submit" class="btn btn-dark btn-block col-md-offset-2">
                      <i class="fa fa-check-square-o"></i> Update
                    </button>
                  </div>

              </div>

            </form>

            </div>
          </div>
               
          </div>
        </div>




      </div><!--End Row-->
	  
       <!--End Dashboard Content-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   

  @include('expert.copyright')

  @endsection

  @section('js')
    <script>
    $(document).ready(function() {
        dataTableLoad({
            curUrl: "{{route('Admin.userrole.index')}}",
            addUrl: "{{route('Admin.userrole.create')}}"
        });
    });
    </script>
  @endsection
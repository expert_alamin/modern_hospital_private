<head>
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
  <meta name="description" content=""/>
  <meta name="author" content=""/>
  <meta name="csrf-token" content="siQAzC5MB0KAaw3OMNuipUFVbuRGJiPjNXzStNzI">
<title> Medicine Purchase Invoice - {{$bill_manage->bill_id}} - {{$settingsinfo->company_name}} </title>
  <!--favicon-->
  <link rel="icon" type="image/png" href="{{ asset('/logo/158116565711.png') }}" />
  
<style>
body{
font-family:Verdana, Arial, Helvetica, sans-serif;
}

.sagor{
  font-size:10px;
  }

@media print {
  
.noprint{
  display: none;
}

#sidebar-wrapper,.pb-2,.footer,.no-print {visibility: hidden;}
.content-wrapper,.card-body {margin-left: 0px;}
/*.content-wrapper {margin-top: 45px;}*/
html, body {
  background: #fff;
  font-family: 'Poppins', sans-serif;
  color: #636363;
  letter-spacing: 1px;
  position: relative;
  margin: 0px auto; 
  font-size: 10px !important;
  width: 300px !important;
}

}



</style>

</head>

<body>

<div align="center" class="sagor">
  <table width="300" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="280" height="50" align="center" valign="middle" style="font-size:16px; font-weight:bold;">Modern Hospital Private</td>
      <td width="20">&nbsp;</td>
    </tr>
    <tr>
      <td height="10">        </td>
      <td></td>
    </tr>
    <tr>
      <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td height="50" colspan="2" align="center" valign="middle" class="sagor" style="font-size:16px; font-weight:bold;">Medicine Purchase Bill </strong></td>
          </tr>
        <tr>
          <td width="50%" height="30" class="sagor">Bill ID : <strong><strong>{{$bill_manage->bill_id}}</strong></td>
          <td width="50%" class="sagor">Date : <strong><strong>{{$bill_manage->date}}</strong></td>
        </tr>
        <tr>
          <td height="30" class="sagor">Supplier : <strong>{{$bill_manage->supplier_name}}</strong></td>
          <td class="sagor">Phone : <strong><strong>{{$bill_manage->supplier_phone}}</strong></td>
        </tr>
        <tr>
          <td height="30" class="sagor">Product Category : <strong>{{$bill_manage->product_category}}</strong></td>
          <td class="sagor">Medicine Company : <strong>{{$bill_manage->company_name}}</strong></td>
        </tr>
        
        
      <tr>
          <td height="30" class="sagor"></td>
          <td height="30" class="sagor">Total Amount : <strong>{{$bill_manage->total}} TK </strong></td>
      </tr>
      
       <tr>
          <td height="30">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      
      </table></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
       <td height="10" >

              
   <table width="100%" border="0" cellspacing="2" cellpadding="2">
         <tr>
           <td width="5%" height="30" align="center" valign="middle" style="border-top: 1px solid #000;border-bottom: 1px solid #000;" class="sagor"><strong>#</strong></td>
           <td width="10%" align="center" valign="middle" style="border-top: 1px solid #000;border-bottom: 1px solid #000;" class="sagor"><strong>Serial</strong></td>
           <td width="40%" align="left" valign="middle" style="border-top: 1px solid #000;border-bottom: 1px solid #000;" class="sagor"><strong>Medicine   </strong></td>
       <td width="10%" align="center" valign="middle" style="border-top: 1px solid #000;border-bottom: 1px solid #000;" class="sagor"><strong>QTY</strong></td>
           <td width="10%" align="center" valign="middle" style="border-top: 1px solid #000;border-bottom: 1px solid #000;" class="sagor"><strong>Buy Price</strong></td>
           <td width="15%" align="center" valign="middle" style="border-top: 1px solid #000;border-bottom: 1px solid #000;" class="sagor"><strong>Buy Total Amount</strong></td>
           <td width="10%" align="center" valign="middle" style="border-top: 1px solid #000;border-bottom: 1px solid #000;" class="sagor"><strong>Buy Sell Price</strong></td>
          </tr>
          @php
            $i=1;

          @endphp

          @foreach($pha_pur_add_to_cart as $data)
         <tr>
           <td height="30" align="center" valign="middle" style="border-bottom: 1px solid #000;" class="sagor">
            {{$i++}}           </td>
           <td align="center" valign="middle" style="border-bottom: 1px solid #000;" class="sagor">{{$data->serial_number}} </td>
           <td align="left" valign="middle" style="border-bottom: 1px solid #000;" class="sagor">{{$data->medicine_name}} </td>
           <td align="center" valign="middle" style="border-bottom: 1px solid #000;" class="sagor">{{$data->qty}} </td>
           <td align="center" valign="middle" style="border-bottom: 1px solid #000;" class="sagor">{{$data->buy_price}} </td>
           <td align="center" valign="middle" style="border-bottom: 1px solid #000;" class="sagor">{{$data->totay_buy_price}} </td>
           <td align="center" valign="middle" style="border-bottom: 1px solid #000;" class="sagor">{{$data->sell_price}} </td>
           
          </tr>
          @endforeach
         
          
          <tr>
           <td height="30" align="center" valign="middle" colspan="4">&nbsp;</td>
           <td align="right" valign="middle" class="sagor">
             <strong> Total : </strong>           </td>
           <td align="center" valign="middle" style="border-bottom: 1px solid #000;" class="sagor"><strong> {{$bill_manage->total}}  TK </strong></td>
       <td height="30" align="center" valign="middle" >&nbsp;</td>
          </tr>
          
       </table>     
     </td>
       <td >&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr >
      <td height="50">&nbsp;</td>
      <td >&nbsp;</td>
    </tr>
  <tr class="noprint">
      <td height="50">

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="center" valign="middle">
            <a href="{{url('admin/phamedpur')}}" style="background: #db5246; border: 1px solid #db5246; padding:15px; color: #ffffff; text-decoration: none; ">
                <i class="fa fa-check-square-o"></i> Back            </a>          </td>
          <td align="center" valign="middle">&nbsp;</td>
          <td align="center" valign="middle">
            <button onClick="window.print()" style="background: #129a5a; border: 1px solid #129a5a; padding:15px; color: #ffffff; text-decoration: none;cursor: pointer;font-size: 18px;">
                <i class="fa fa-check-square-o"></i> Print            </button>          </td>
        </tr>
      </table>    </td>
      <td>&nbsp;</td>
  </tr>
  </table>
</div>


</body>
</html>

@extends('expert.master')

@section('title', 'Investigation Manage - '.$settingsinfo->company_name.' - '.$settingsinfo->soft_name.'')

@section('content')

@include('expert.sidebar')

@include('expert.topbar')

<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">

          <?php if (session('message')): ?>
              <div class="alert alert-{{session('class')}} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <div class="alert-icon contrast-alert"><i class="icon-close"></i></div>
                <div class="alert-message"><span>{{session('message')}}</span></div>
              </div>
        <?php endif; ?>

          <div class="card bg-dark">
      		<div class="card-header border-0 bg-transparent text-white">
                <i class="fa fa-user-circle"></i><span> Investigation Manage</span>
            </div>

            <div class="card">
            <div class="card-header">

              <div style="display:inline-block; padding-top:5px;">
                <i class="fa fa-table"></i> Investigation List
              </div> 

              <a href="{{url('admin/investigationsadd')}}" class="btn btn-dark btn-sm waves-effect waves-light pull-right"> 
                <i class="fa fa-plus"></i> <span>Add New</span>
              </a>

            </div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="dataTable" class="table table-bordered">
                <thead>
                    <tr>
                        <th width="5%">SN</th>
                        <th>Code</th>
                        <th>Investigation Name</th>
                        <th>Unit </th>
                        <th>Normal Value </th>
                        <th>Report File Name</th>
                        <th>Report Days</th>
                        <th>Charge</th>
                        <th>Discount</th>
                        <th width="8%" class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i=1; @endphp
                    @foreach($investigation_manage as $data)
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{$data->code_test}}</td>
                        <td>{{$data->inv_name}}</td>
                        <td>{{$data->unit_test}}</td>
                        <td><?php echo $data->inv_ref_value; ?></td>
                        <td>{{$data->report_file_name}}</td>
                        <td>{{$data->report_days}}</td>
                        <td>{{$data->charge}} TK </td>
                        <td>{{$data->discount}} % </td>
                        <td>

                          <a href="{{url('admin/invedit',$data->id)}}" class="btn btn-warning btn-sm waves-effect waves-light" data="{{$data->id}}"> <i class="fa fa-edit"></i> <span></span></a> 

                          <a href="{{url('admin/investigationsdelete',$data->id)}}" class="btn btn-danger btn-sm waves-effect waves-light"> 
                            <i class="fa fa-times"></i> <span></span>
                          </a>

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
            </div>
          </div>
               
          </div>
        </div>
      </div><!--End Row-->
	  
       <!--End Dashboard Content-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   

  @include('expert.copyright')

  @endsection

  @section('js')
    <script>
    $(document).ready(function() {
        dataTableLoad({
            curUrl: "{{route('Admin.userrole.index')}}",
            addUrl: "{{route('Admin.userrole.create')}}"
        });
    });
    </script>
  @endsection
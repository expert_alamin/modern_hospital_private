@extends('expert.master')

@section('title', 'Test Result Add - '.$settingsinfo->company_name.' - '.$settingsinfo->soft_name.'')

@section('content')

@include('expert.sidebar')

@include('expert.topbar')



<style type="text/css">

.table-responsive {
    white-space: normal;
}
.dataTables_length{
  display: none;
}
</style>

<div class="clearfix"></div>
  
  <div class="content-wrapper">
    <div class="container-fluid">
      <div class="row">

        <?php if (session('message')): ?>
        <div class="col-lg-12">
            <div class="alert alert-{{session('class')}} alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <div class="alert-icon contrast-alert"><i class="icon-close"></i></div>
              <div class="alert-message"><span>{{session('message')}}</span></div>
            </div>
        </div>
        <?php endif; ?>

        <div class="col-lg-8">

          <div class="card bg-dark">
          <div class="card-header border-0 bg-transparent text-white">
                <i class="fa fa-list"></i><span>  Test ID : {{$data->id}} | Inv. Name : {{$data->charge_name}} </span>
            </div>

            <div class="card">
            <div class="card-header">
             Patient : {{$data->name}} ({{$data->pat_id}})
            </div>
            
            <div class="card-body">

              <form action="{{url('admin/testressubac',$data->id)}}" id="qcat" method="post">
              @csrf

              <div class="row">

                  <div class="col-md-12">
                      <div class="form-group">
                          <label for="name">Test Result  </label>
                          <textarea class="form-control" id="report_result" name="report_result"></textarea>
                      </div>
                  </div>

                  <div class="col-md-6">
                      <div class="form-group">
                          <label for="name"><u> Unit test </u> </label>

                          <br>
                          {{$investigation_data->unit_test}}
                          
                          
                          
                      </div>
                  </div>
                  <div class="col-md-6">
                      <div class="form-group">
                          <label for="name"><u> inv reference value </u> </label>
                         <?php echo $investigation_data->inv_ref_value; ?>
                      </div>
                  </div>

                

                  

                  <div class="col-md-12">
                  </div>
                  
                  <div class="col-md-6">
                    <a href="{{url('admin/testlist')}}" class="btn btn-dark btn-block col-md-offset-2">
                      <i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i> Back
                    </a>
                  </div>

                  <div class="col-md-6">
                    <button type="submit" class="btn btn-dark btn-block col-md-offset-2">
                      <i class="fa fa-check-square-o"></i> Save Result
                    </button>
                  </div>

              </div>

            </form>

            </div>
          </div>
               
          </div>
        </div>

        

      </div><!--End Row-->
    
       <!--End Dashboard Content-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   

  @include('expert.copyright')

  @endsection

  @section('js')

<script src="{{ asset('/expert/assets/plugins/summernote/dist/summernote-bs4.min.js') }}"></script>
<script>
$('#report_result').summernote({
  height: 200,   
    codemirror: {
    theme: 'monokai'
  }
});
</script>
  @endsection
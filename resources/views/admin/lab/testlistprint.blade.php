@extends('expert.master')

@section('title', 'Lab Test Manage - '.$settingsinfo->company_name.' - '.$settingsinfo->soft_name.'')

@section('content')

@include('expert.sidebar')

@include('expert.topbar')

<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">

          <?php if (session('message')): ?>
              <div class="alert alert-{{session('class')}} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <div class="alert-icon contrast-alert"><i class="icon-close"></i></div>
                <div class="alert-message"><span>{{session('message')}}</span></div>
              </div>
        <?php endif; ?>

          <div class="card bg-dark">
      		<div class="card-header border-0 bg-transparent text-white">
                <i class="fa fa-user-circle"></i><span> Lab Test Manage</span>
            </div>

            <div class="card">
            <div class="card-header">

              <div style="display:inline-block; padding-top:5px;">
                <i class="fa fa-table"></i> Lab Test List
              </div> 

              

            </div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="dataTable" class="table table-bordered">
                <thead>
                    <tr>
                        <th width="5%">Test ID</th>
                        <th>Bill ID</th>
                        <th>Patient Name</th>
                        <th>Status</th>
                        
                        <th width="8%" class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i=1; @endphp
                    @foreach($testlistprint as $data)
                    <tr>
                        <td>{{$data->id}}</td>
                        <td>{{$data->bill_id}}</td>
                        <td>{{$data->name}}</td>
                        <td>
                          @if(empty($data->report_status))
                          Pending 
                          @else
                          {{$data->report_status}}
                          @endif
                        </td>
                        
                        <td>
                          @if(empty($data->report_status))
                            <a href="{{url('admin/testressub',$data->id)}}" class="btn btn-success btn-sm waves-effect waves-light"> 
                            <i class="fa fa-clipboard"></i> <span> 
                            Submit Report 
                          </span>
                          </a> 
                          @else
                            <a href="{{url('admin/testalllistresprint',$data->bill_id)}}" class="btn btn-warning btn-sm waves-effect waves-light"> 
                            <i class="fa fa-print"></i> <span> 
                            view Report 
                          </span>
                          </a>
                          @endif
                          

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
            </div>
          </div>
               
          </div>
        </div>
      </div><!--End Row-->
	  
       <!--End Dashboard Content-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   

  @include('expert.copyright')

  @endsection

  @section('js')
    <script>
    $(document).ready(function() {
        dataTableLoad({
            curUrl: "{{route('Admin.userrole.index')}}",
            addUrl: "{{route('Admin.userrole.create')}}"
        });
    });
    </script>
  @endsection
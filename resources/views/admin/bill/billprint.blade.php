<head>
  <meta charset="utf-8"/>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
  <meta name="description" content=""/>
  <meta name="author" content=""/>
  <meta name="csrf-token" content="siQAzC5MB0KAaw3OMNuipUFVbuRGJiPjNXzStNzI">
<title> Invoice - MHP-{{$bill_manage->bill_id}} - {{$settingsinfo->company_name}} </title>
  <!--favicon-->
  <link rel="icon" type="image/png" href="{{ asset('/logo/158116565711.png') }}" />
  
<style>
body{
font-family:Verdana, Arial, Helvetica, sans-serif;
}

.sagor{
  font-size:10px;
  }

@media print {
.noprint{
  display: none;
}
}



</style>

</head>

<body>

<div align="center" class="sagor">
  <table width="560" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="520" height="100">&nbsp;</td>
      <td width="40">&nbsp;</td>
    </tr>
    <tr>
      <td height="10">        </td>
      <td></td>
    </tr>
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="50%" height="30" class="sagor">Bill ID : <strong><strong>{{$bill_manage->bill_id}}</strong></td>
          <td width="50%" class="sagor">Date : <strong><strong>{{$bill_manage->date_time}}</strong></td>
        </tr>
        <tr>
          <td height="30" class="sagor">Patient : <strong>{{$bill_manage->name}} ({{$bill_manage->pat_id}})</strong></td>
          <td class="sagor">Age/Sex : <strong><strong>{{$bill_manage->age}}/{{$bill_manage->gender}}</strong></td>
        </tr>
        <tr>
          <td height="30" class="sagor">Mobile : <strong>{{$bill_manage->phone}}</strong></td>
          <td class="sagor">Patient Type : <strong>{{$bill_manage->pat_type}}</strong></td>
        </tr>
        
        <tr>
          <td height="30" colspan="2" class="sagor">
        @if(!empty($bill_manage->visit_dr))
            Consultant : <strong>{{$bill_manage->visit_dr}}</strong> <strong>, {{$settingsinfo->company_name}}</strong>
            @endif      </td>
          </tr>
      
      <tr>
          <td height="30" class="sagor">
         @if(!empty($bill_manage->bed_id))
            Bed : <strong>{{$bill_manage->bed_name}}, {{$bill_manage->bed_type}} , {{$bill_manage->floor}}</strong>
            @endif      </td>
          <td height="30" class="sagor">Total Bill : <strong>{{$bill_manage->grand_total}} TK </strong></td>
      </tr>
      
       <tr>
          <td height="30">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      
      </table></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
       <td height="10" >

              
   <table width="100%" border="0" cellspacing="2" cellpadding="2">
         <tr>
           <td width="5%" height="30" align="center" valign="middle" style="border-top: 1px solid #000;border-bottom: 1px solid #000;" class="sagor"><strong>SN</strong></td>
           <td width="70%" align="left" valign="middle" style="border-top: 1px solid #000;border-bottom: 1px solid #000;" class="sagor"><strong>Charge Details </strong></td>
           <td width="25%" align="center" valign="middle" style="border-top: 1px solid #000;border-bottom: 1px solid #000;" class="sagor"><strong>Total</strong></td>
          </tr>
          @php
            $i=1;
          @endphp

          @foreach($bill_add_to_cart as $data)
         <tr>
           <td height="30" align="center" valign="middle" style="border-bottom: 1px solid #000;" class="sagor">
            {{$i++}}           </td>
           <td align="left" valign="middle" style="border-bottom: 1px solid #000;" class="sagor">{{$data->charge_name}} </td>
           <td align="center" valign="middle" style="border-bottom: 1px solid #000;" class="sagor">
            {{$data->total}}  TK           </td>
          </tr>
          @endforeach
         <tr>
           <td height="30" align="center" valign="middle" >&nbsp;</td>
           <td align="right" valign="middle" class="sagor">
             <strong>Sub Total : </strong>           </td>
           <td align="center" valign="middle" style="border-bottom: 1px solid #000;" class="sagor"><strong> {{$bill_manage->total}}  TK </strong></td>
          </tr>
          <tr>
           <td height="30" align="center" valign="middle" >&nbsp;</td>
           <td align="right" valign="middle" class="sagor">
             <strong>Discount : </strong>           </td>
           <td align="center" valign="middle" style="border-bottom: 1px solid #000;" class="sagor"><strong> {{$bill_manage->discount}}  TK </strong></td>
          </tr>
          <tr>
           <td height="30" align="center" valign="middle" >&nbsp;</td>
           <td align="right" valign="middle" class="sagor">
             <strong>Grand Total : </strong>           </td>
           <td align="center" valign="middle" style="border-bottom: 1px solid #000;" class="sagor"><strong> {{$bill_manage->grand_total}}  TK </strong></td>
          </tr>
          <tr>
           <td height="30" align="center" valign="middle" >&nbsp;</td>
           <td align="right" valign="middle" class="sagor">
             <strong>Payment : </strong>           </td>
           <td align="center" valign="middle" style="border-bottom: 1px solid #000;" class="sagor"><strong> {{$bill_manage->payment}}  TK </strong></td>
          </tr>
          <tr>
           <td height="30" align="center" valign="middle" >&nbsp;</td>
           <td align="right" valign="middle" class="sagor">
             <strong>Due : </strong>           </td>
           <td align="center" valign="middle" style="border-bottom: 1px solid #000;" class="sagor"><strong> {{$bill_manage->due}}  TK </strong></td>
          </tr>
       </table>     </td>
       <td >&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr >
      <td height="50">&nbsp;</td>
      <td >&nbsp;</td>
    </tr>
  <tr class="noprint">
      <td height="50">

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="center" valign="middle">
            <a href="{{url('admin/billadd')}}" style="background: #db5246; border: 1px solid #db5246; padding:15px; color: #ffffff; text-decoration: none; ">
                <i class="fa fa-check-square-o"></i> Back            </a>          </td>
          <td align="center" valign="middle">&nbsp;</td>
          <td align="center" valign="middle">
            <button onclick="window.print()" style="background: #129a5a; border: 1px solid #129a5a; padding:15px; color: #ffffff; text-decoration: none;cursor: pointer;font-size: 18px;">
                <i class="fa fa-check-square-o"></i> Print            </button>          </td>
        </tr>
      </table>    </td>
      <td>&nbsp;</td>
  </tr>
  </table>
</div>


</body>
</html>

@extends('expert.master')

@section('title', 'Bill Manage - '.$settingsinfo->company_name.' - '.$settingsinfo->soft_name.'')

@section('content')

@include('expert.sidebar')

@include('expert.topbar')

<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">

          <?php if (session('message')): ?>
              <div class="alert alert-{{session('class')}} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <div class="alert-icon contrast-alert"><i class="icon-close"></i></div>
                <div class="alert-message"><span>{{session('message')}}</span></div>
              </div>
        <?php endif; ?>

          <div class="card bg-dark">
      		<div class="card-header border-0 bg-transparent text-white">
                <i class="fa fa-user-circle"></i><span> Bill Manage</span>
            </div>

            <div class="card">
            <div class="card-header">

              <div style="display:inline-block; padding-top:5px;">
                <i class="fa fa-table"></i> Bill List
              </div> 

              <a href="{{url('admin/investigationsadd')}}" class="btn btn-dark btn-sm waves-effect waves-light pull-right"> 
                <i class="fa fa-plus"></i> <span>Add New</span>
              </a>

            </div>
            <div class="card-body">
              <div class="table-responsive">
              <table id="dataTable" class="table table-bordered">
                <thead>
                    <tr>
                        <th width="5%">SN</th>
                        <th>Bill ID</th>
                        <th>Date</th>
                        <th>G. Total</th>
                        <th>Payment</th>
                        <th>Due</th>
                        <th>Status</th>
                        <th width="8%" class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @php $i=1; @endphp
                    @foreach($bill_manage as $data)
                    <tr>
                        <td>{{$i++}}</td>
                        <td>{{$data->bill_id}}</td>
                        <td>{{$data->date_time}}</td>
                        <td>{{$data->grand_total}} TK </td>
                        <td>{{$data->payment}} TK </td>
                        <td>{{$data->due}} TK </td>
                        <td>{{$data->status}}</td>
                        <td>

                          <!-- <button type="button" class="btn btn-warning btn-sm waves-effect waves-light edit" data="{{$data->id}}"> <i class="fa fa-edit"></i> <span></span></button> --> 
                          @if($data->status == "Due")
                          <a href="{{url('admin/billpayment',$data->bill_id)}}" class="btn btn-success btn-sm waves-effect waves-light"> 
                            <i class="fa fa-credit-card"></i> <span> Payment </span>
                          </a>
                          @endif

                          <a href="{{url('admin/billprint',$data->bill_id)}}" class="btn btn-warning btn-sm waves-effect waves-light"> 
                            <i class="fa fa-print"></i> <span> Print Bill</span>
                          </a>

                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            </div>
            </div>
          </div>
               
          </div>
        </div>
      </div><!--End Row-->
	  
       <!--End Dashboard Content-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   

  @include('expert.copyright')

  @endsection

  @section('js')
    <script>
    $(document).ready(function() {
        dataTableLoad({
            curUrl: "{{route('Admin.userrole.index')}}",
            addUrl: "{{route('Admin.userrole.create')}}"
        });
    });
    </script>
  @endsection
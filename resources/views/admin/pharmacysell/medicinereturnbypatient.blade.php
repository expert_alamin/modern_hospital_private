@extends('expert.master')

@section('title', 'Merchant Payment Report- '.$settingsinfo->company_name.' - '.$settingsinfo->soft_name.'')

@section('content')

@include('expert.sidebar')

@include('expert.topbar')

<style type="text/css">

.table-responsive {
    white-space: normal;
}
.dataTables_length{
  display: none;
}
</style>

<div class="clearfix"></div>
	
  <div class="content-wrapper">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <div class="card bg-dark">
      		<div class="card-header border-0 bg-transparent text-white">
                <i class="fa fa-list"></i><span> Return Medicine </span>
            </div>

            <div class="card">
            <div class="card-header">
              
              <?php if (session('message')): ?>
        <div class="col-lg-12">
            <div class="alert alert-{{session('class')}} alert-dismissible" role="alert">
              <button type="button" class="close" data-dismiss="alert">×</button>
              <div class="alert-icon contrast-alert"><i class="icon-close"></i></div>
              <div class="alert-message"><span>{{session('message')}}</span></div>
            </div>
        </div>
        <?php endif; ?>

              <form action="{{url('admin/medicinereturnbypatientbillview')}}" method="post">
              @csrf


              <div class="row">

                <div class="col-md-6">         
                  <div class="form-group" style="margin-bottom: 0rem;">
                    
                    <select type="text" class="form-control" id="patient_id" name="patient_id" required="">
                        <option value="">Select Patient ID</option>
 
                        @foreach($patient_info as $data)
                          <option value="{{$data->pat_id}}">{{$data->pat_id}} </option>
                        @endforeach

                    </select>
                    
                  </div>
                </div>


                <div class="col-md-6">         
                  <div class="form-group pull-right" style="margin-bottom: 0rem;">
             
                    <button type="submit" class="btn btn-gradient-orange waves-effect waves-light m-1" style="margin-top: 0px !important;">
                      View 
                    </button>
                  </div>
                </div>

              </form>
            </div>
            </div>
          
            
            <div class="card-body">

            
            </div>
          </div>
               
          </div>
        </div>
      </div><!--End Row-->
	  
       <!--End Dashboard Content-->

    </div>
    <!-- End container-fluid-->
    
    </div><!--End content-wrapper-->
   

  @include('expert.copyright')

  @endsection

  @section('js')
    <script>
    $(document).ready(function() {
        dataTableLoad({
            curUrl: "{{route('Admin.usermanage.index')}}",
            //dataUrl: "{{route('Admin.usermanage.index')}}",
            addUrl: "{{route('Admin.usermanage.index')}}"
        });
    });

    $(document).ready(function() {
      $("#patient_id").select2();
    });
    </script>
  @endsection